import cors = require('cors')
import fs from "fs"
import path from "path"
import PgPubsub from '@graphile/pg-pubsub'
import jwt from 'express-jwt'
import * as jwksRsa from 'jwks-rsa'
import jwtDecode from 'jwt-decode'
import {
    GraphQLErrorExtended,
    makePluginHook,
    postgraphile,
} from 'postgraphile'
import { extendedFormatError } from 'postgraphile/build/postgraphile/extendedFormatError'
import logger from '@shared/Logger';

import bodyParser = require('body-parser')
import { IncomingMessage, ServerResponse } from 'http'
import { isGitpod } from './shared/gitpod'


const PostGraphileUploadFieldPlugin = require("postgraphile-plugin-upload-field")
const { graphqlUploadExpress } = require("graphql-upload")



export type JwtClaims = {[claimName: string]: any }

export type CytonContext = {
    user?: {
        jwtClaims: JwtClaims
    },
    userHasRole: (role: string) => boolean
}

export function useCytonContext(context: any) : CytonContext {
    return context.cyton as CytonContext
}


export type ApiOptions = {
    postgresUrl: string
    role: string
    schemas: string[]
    corsAllowed: string[]
    baseAuthUrl: string
    realm: string
    disableDefaultMutations?: boolean
    plugins?: any[]
}

export async function initApi(
    apiOptions: ApiOptions,
    app: any 
) {
    const {
        postgresUrl,
        baseAuthUrl,
        corsAllowed,
        disableDefaultMutations,
        realm,
        role,
        schemas,
    } = apiOptions

    
    const strictSsl = !isGitpod()

    // TODO FIXME reenable checkJwt after resolving gitpod request header issues
    // const checkJwt = jwt({
    //     secret: jwksRsa.expressJwtSecret({
    //         cache: true,
    //         rateLimit: true,
    //         jwksRequestsPerMinute: 5,
    //         jwksUri: `${baseAuthUrl}/realms/${realm}/protocol/openid-connect/certs`,
    //         strictSsl
    //     }),
    //     audience: `account`,
    //     issuer: `${baseAuthUrl}/realms/${realm}`,
    //     algorithms: ['RS256'],
    //     credentialsRequired: false,
    // })

    logger.info(`CORS origins: ${corsAllowed}`)
    const corsOptions = {
        origin: corsAllowed,
        credentials: true,

        allowedHeaders: [
            'Origin',
            'X-Requested-With',
            // Used by `express-graphql` to determine whether to expose the GraphiQL
            // interface (`text/html`) or not.
            'Accept',
            // Used by PostGraphile for auth purposes.
            'Authorization',
            // Used by GraphQL Playground and other Apollo-enabled servers
            'X-Apollo-Tracing',
            // The `Content-*` headers are used when making requests with a body,
            // like in a POST request.
            'Content-Type',
            'Content-Length',
            // For our 'Explain' feature
            'X-PostGraphile-Explain',
        ],
    }
    const withCors = cors(corsOptions)


    // TODO make this version work again:
    // const getPgSettings = async (request: any) => {
    //     const settings: any = {}

    //     settings[`role`] = role
    //     settings[`search_path`] = schemas.join(`,`)
    //     settings[`cyton.user_session.jwt`] = JSON.stringify(request.user || {})

    //     logger.info(`Using PgSettings: ${JSON.stringify(settings)}`)
    //     return settings
    // }

    // TODO find a better way than using cookies like this
    const bearerOffset = 'bearer '.length
    const getJwtFromRequest = (request: any) : any => {
        let auth: string = request.headers[`authorization`]
        logger.info(`using auth token from headers: ${auth}`)

        if (auth && auth.trim().length > bearerOffset) {
            const token = auth.trim().substr(bearerOffset)
            return jwtDecode(token)
        }
        
        const token = request.cookies.cyton_jwt || ``
        if (token) {
            logger.info(`using auth token from session: ${token}`)
            return jwtDecode(token)
        }
        
        logger.info(`no auth token found`)
        return null
    }

    const getPgSettings = async (request: any) => {
        const jwt = getJwtFromRequest(request)
        const settings: any = {}
        settings[`role`] = role
        settings[`search_path`] = schemas.join(`,`)
        if (jwt) {
            settings[`cyton.user_session.jwt`] = JSON.stringify(jwt)
        }
        logger.info(`Using PgSettings: ${JSON.stringify(settings)}`)
        return settings
    }



    const additionalGraphQLContextFromRequest = async (
        request: IncomingMessage,
        response: ServerResponse
    ) => {
        // const jwtClaims = (request as any).user
        const jwtClaims = getJwtFromRequest(request)
        const cyton: CytonContext = {
            user: {
                jwtClaims,
            },
            userHasRole: (role: string) =>  {
                const claims = cyton.user?.jwtClaims
                if (claims) {
                    return claims.realm_access.roles.indexOf(role) !== -1
                }
                return false
            }
        }

        return { cyton }
    }

    const PgSimplifyInflectorPlugin = require('@graphile-contrib/pg-simplify-inflector')
    const SubscriptionPlugin = require('@graphile/subscriptions-lds').default

    const pluginHook = makePluginHook([ PgPubsub])

    const extendedErrors = [
        'severity',
        'code',
        'detail',
        'hint',
        'position',
        'internalPosition',
        'internalQuery',
        'where',
        'schema',
        'table',
        'column',
        'dataType',
        'constraint',
        'file',
        'line',
        'routine',
    ]

    const formatError = (error: any) => {
        const formattedError: any = extendedFormatError(error, extendedErrors)
        formattedError['stack'] = error.stack
        logger.err(JSON.stringify(formattedError))
        return formattedError as GraphQLErrorExtended
    }

    // TODO FIXME reenable SubscriptionPlugin after getting wal2jsoon working
    //let plugins = [PgSimplifyInflectorPlugin, SubscriptionPlugin]
    let plugins = [PgSimplifyInflectorPlugin, PostGraphileUploadFieldPlugin]
    if (apiOptions.plugins) {
        plugins = [...plugins, ...apiOptions.plugins]
    }

    app.use(graphqlUploadExpress());


    // Apply CORS to the graphql endpoint
    app.options('/graphql', withCors)

    
    app.use(
        withCors,
        //checkJwt,
        postgraphile(postgresUrl, schemas, {
            additionalGraphQLContextFromRequest,
            disableDefaultMutations,
            pluginHook,
            subscriptions: true, // start the websocket server
            simpleSubscriptions: true, // Add the `listen` subscription field
            dynamicJson: true,
            live: true,
            watchPg: true,
            graphiql: true,
            enhanceGraphiql: true,
            ignoreRBAC: false,
            pgSettings: getPgSettings,
            handleErrors: (errors) => {
                return errors.map(formatError)
            },
            ownerConnectionString: postgresUrl,
            appendPlugins: plugins,
            graphileBuildOptions: {
                uploadFieldDefinitions: [
                    {
                    match: ({ column }: any) => column === "upload_file",
                        resolve: resolveUpload,
                    },
                ],
            },
        })
    )
}

async function resolveUpload(upload:any) {
 logger.info(`resolving upload`)
  const { filename, mimetype, encoding, createReadStream } = upload;
  logger.info(`createReadStream() for filename ${filename}`)
  const stream = createReadStream();
  // Save file to the local filesystem
  logger.info(`calling saveLocal()`)
  const { id, path } = await saveLocal({ stream, filename });
  logger.info(`saveLocal() returned id ${id} path ${path}`)
  // Return metadata to save it to Postgres
  return {
    id,
    path,
    filename,
    mimetype,
    encoding
  };
}

function saveLocal({ stream, filename }: any) : Promise<any> {
  const timestamp = new Date().toISOString().replace(/\D/g, "");
  const id = `${timestamp}_${filename}`;
  const filepath = `${id}`
  const fsPath = `/workspace/uploads/${filepath}`
  logger.info(`attempting to save ${id} to ${fsPath}`)
  return new Promise((resolve, reject) =>
    stream
      .on("error", (error:any) => {
        if (stream.truncated)
          // Delete the truncated file
          fs.unlinkSync(fsPath);
        reject(error);
      })
      .on("end", () => resolve({ id, filepath }))
      .pipe(fs.createWriteStream(fsPath))
  );
}