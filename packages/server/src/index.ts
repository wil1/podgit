import './pre-start'; // Must be the first import
import app from '@server';
import logger from '@shared/Logger';
import { initializeDatabase } from './initializers/initDatabase'
import { initApi } from './GraphileServer';
import { getDatabaseUrl } from './shared/db'
import { getCytonAuthDomain } from '@shared/gitpod';
import { ImportUploadPlugin } from './plugins/file/ImportUploadPlugin'

async function start() {
    await initializeDatabase()
    
    const role = `cyton_user`
    const schemas = [`cyton`]
    const baseUrl = process.env.REACT_APP_CYTON_ROOT_URL || ``
    const corsAllowed: string[] = [baseUrl]
    const postgresUrl = getDatabaseUrl()
    const baseAuthUrl = getCytonAuthDomain()
    const realm = process.env.CYTON_PROJECT_NAME || ``
    
    await initApi(
        {
            baseAuthUrl,
            realm,
            postgresUrl,
            corsAllowed,
            role,
            schemas,
            plugins: [
                ImportUploadPlugin
            ],
        },
        app
    )

    // Start the server
    const port = Number(process.env.PORT || 4000)
    app.listen(port, () => {
        logger.info('Express server started on port: ' + port)
    })
}

start()
