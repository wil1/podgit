import * as fs from 'fs'
import * as path from 'path'
import * as mkdirp from 'mkdirp'
import * as util from 'util'
import logger from '@shared/Logger';
import rimraf from 'rimraf'
import { sleep } from './asyncUtil'
import * as crypto from 'crypto'

export const asyncWriteFile = util.promisify(fs.writeFile)

export const asyncUnlink = util.promisify(fs.unlink)

export async function waitForResult<T>(
    producer: () => Promise<T>,
    filter: (content: T) => boolean,
    interval: number = 1000
) {
    let result = false
    while (result === false) {
        const content = await producer()
        logger.info(`waitForResult producer returned content`)
        result = filter(content)
        if (result) {
            logger.info(`content matched filter`)
            return content
        } else {
            logger.info(`content did not match filter, retrying...`)
            await sleep(interval)
        }
        
    }
}

export function waitForFile(filePath: string) {
    return new Promise(function (resolve, reject) {
        const tryRead = () => {
            logger.info(`attempting to read file ${filePath}`)
            fs.readFile(filePath, (err, data) => {
                if (err) {
                    logger.info(`error reading file ${filePath}`)
                    setTimeout(tryRead, 5000)
                } else {
                    logger.info(`Loaded data from file ${filePath}`)
                    resolve(data.toString())
                }
            })
        }

        tryRead()
    })
}


export async function readOrCreate(
    filePath: string,
    loadInitialData: () => Promise<string>
) {
    return new Promise(function (resolve, reject) {
        const onFile = (arg1?: any, arg2?: any) => {
            fs.readFile(filePath, (err, data) => {
                if (err) {
                    logger.err(`error reading file ${filePath}: ${err}`)
                    reject(err)
                } else {
                    const result = data.toString()
                    resolve(result)
                }
            })
        }

        fs.access(filePath, fs.constants.R_OK, function (err) {
            if (err) {
                loadInitialData().then((data) => {
                    fs.writeFile(filePath, data, onFile)
                })
            } else {
                onFile()
            }
        })
    }) as Promise<string>
}

export async function readFile(filePath: string) {
    return new Promise(function (resolve, reject) {
        const onFile = (arg1?: any, arg2?: any) => {
            fs.readFile(filePath, (err, data) => {
                if (err) {
                    logger.err(`error reading file ${filePath}: ${err}`)
                    reject(err)
                } else {
                    const result = data.toString()
                    resolve(result)
                }
            })
        }

        fs.access(filePath, fs.constants.R_OK, function (err) {
            if (err) {
                reject(err)
            } else {
                onFile()
            }
        })
    }) as Promise<string>
}

export function rmrf(path: string) {
    return new Promise(function (resolve, reject) {
        rimraf(path, (e) => {
            if (e) {
                reject(e)
            } else {
                resolve(true)
            }
        })
    })
}

export function exists(path: string) {
    logger.info(`checking if path "${path}" exists`)
    return new Promise(function (resolve, reject) {
        fs.access(path, err => {
            if (err) {
                logger.info(`exists: err: ${err}`)
                resolve(false)
            } else {
                logger.info(`path does exist: "${path}"`)
                resolve(true)
            }
        })
    })
}


export function sha256File(filename: string) {
  return new Promise<string>((resolve, reject) => {
    // Algorithm depends on availability of OpenSSL on platform
    // Another algorithms: 'sha1', 'md5', 'sha256', 'sha512' ...
    let shasum = crypto.createHash(`sha256`);
    try {
      let s = (fs as any).ReadStream(filename)
      s.on(`data`, function (data:any) {
        shasum.update(data)
      })
      // making digest
      s.on('end', function () {
        const hash = shasum.digest('hex')
        return resolve(hash);
      })
    } catch (error) {
      return reject('calc fail');
    }
  });
}


export function getUploadDirectory() {
    const result = process.env.CYTON_UPLOAD_DIRECTORY
    if (result) {
        return result
    }
    throw new Error(`process.env.CYTON_UPLOAD_DIRECTORY is required`)
}


export function getDataDirectory() {
    const result = process.env.CYTON_DATA_DIRECTORY
    if (result) {
        return result
    }
    throw new Error(`process.env.CYTON_DATA_DIRECTORY is required`)
}

export function getFilesDirectory() {
    return getDataDirectory() + `/files`
}


export function getFilePathBySha256(sha256: string) {
    const dir = getFilesDirectory()
    
    const pathChunks = 8
    const chunkSize = 2
    
    const pathParts = [dir]
    
    for (var i=0; i<pathChunks*chunkSize; i+=chunkSize) {
        pathParts.push(sha256.substring(i, i+chunkSize))
    } 
    pathParts.push(sha256.substring(pathChunks*chunkSize))
    return pathParts.join(`/`)
}
