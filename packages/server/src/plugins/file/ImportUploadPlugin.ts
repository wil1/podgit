import * as fs from 'fs/promises'
import logger from '@shared/Logger'
import { gql, makeExtendSchemaPlugin } from 'graphile-utils'
import {  useCytonContext  } from '../../GraphileServer'
import mkdirp from 'mkdirp'
import { getFilePathBySha256, getUploadDirectory, sha256File } from '@shared/file'

const uploadDirectory = getUploadDirectory()

export const ImportUploadPlugin = makeExtendSchemaPlugin((build) => ({
    typeDefs: gql`
        input ImportUploadFileInput {
            uploadFileId: Int!
            fileId: Int
        }

        type ImportUploadFilePayload {
            fileId: Int!
            query: Query
        }

        extend type Mutation {
            importUploadFile(input: ImportUploadFileInput!): ImportUploadFilePayload
        }
    `,

    resolvers: {
        Mutation: {
            importUploadFile: async (_query, args, context) => {
                const { pgClient } = context
                const {user, userHasRole} = useCytonContext(context)
                const {uploadFileId, fileId } = args.input
                let result = -1
                
                if (user) {
                
                    await pgClient.query(`SAVEPOINT graphql_mutation`);
                    
                    try {
                        const { rows } = await pgClient.query(`select upload_file From cyton.file_uploads where id = $1`, [uploadFileId])
                        const uploadMetadata = rows[0]?.upload_file
                        const { filename, id } = uploadMetadata
                        
                        const uploadedFileName = `${uploadDirectory}/${id}`
                        const sha = await sha256File(uploadedFileName)
                        logger.info(`SHA256 for file ${filename} is ${sha}`)
                        
                        const result = await pgClient.query(`
                            insert into cyton.files
                                (name, sha256, file_metadata)
                            values ($1, $2, $3)
                            returning id
                        `, [filename, sha, uploadMetadata])
                        
                        logger.info(`cyton.files.id = ${result}`)

                        // TODO move file to folder
                        const targetDir = getFilePathBySha256(sha)
                        logger.info(`preparing to move ${id} to ${targetDir}`)
                        await mkdirp(targetDir)
                        await fs.rename(uploadedFileName, `${targetDir}/${filename}`)
                        logger.info(`renamed to ${targetDir}/${filename}`)
                        
                        // const projectGroupId = result?.rows[0].id
                        
                        // await pgClient.query(`
                        //     insert into cyton_hidden.project_group_members
                        //         (project_group_id, user_id, role)
                        //     values ($1, cyton.current_user_id(), 'admin')
                        // `, [projectGroupId])
                        

                        // await pgClient.query(`
                        //     insert into cyton_hidden.project_group_members
                        //         (project_group_id, user_id, role)
                        //     values ($1, cyton.current_user_id(), 'admin')
                        // `, [projectGroupId])
                        
                        // await addRoute(`auth`, dockerNames.containerName(projectGroupId, `keycloak`), 8080)
                        // await addRoute(`pgadmin`, dockerNames.containerName(projectGroupId, `pgadmin`), 80)
                        // await addRoute(`files`, dockerNames.containerName(projectGroupId, `files-gatekeeper`), 3000)
                        

                        // const tmpDir = dockerNames.tmpCredentialsDir(projectGroupId)
                        // await rmrf(tmpDir)
                        // await mkdirp(tmpDir)
                        // await readOrCreate(`${tmpDir}/adminAccountName`, async ()=> adminAccountName)
                        // await readOrCreate(`${tmpDir}/adminInitialPassword`, async ()=> adminInitialPassword)
                        // await readOrCreate(`${tmpDir}/adminFirstName`, async ()=> adminFirstName)
                        // await readOrCreate(`${tmpDir}/adminLastName`, async ()=> adminLastName)
                        

                    } catch (e) {
                        await pgClient.query(`ROLLBACK TO SAVEPOINT graphql_mutation`);
                        throw e;
                    } finally {
                        await pgClient.query(`RELEASE SAVEPOINT graphql_mutation`);
                    }

                }

                return {
                    fileId: result,
                    query: build.$$isQuery,
                }


            },
        },
    },
}))




                // const {name, baseDomain, description, adminAccountName, adminInitialPassword, adminFirstName, adminLastName} = args.input
        
                // const addRoute = async (
                //     subdomain: string,
                //     internalHost: string,
                //     internalPort: number
                // ) => {
                //     const externalHost = `${subdomain}.${baseDomain}`
                //     await pgClient.query(`
                //     insert into cyton.proxy_routes 
                //         (external_host_name, internal_host_name, internal_port) 
                //     values
                //         ($1, $2, $3)
                //     `, [externalHost, internalHost, internalPort])
                    
                // }
        
