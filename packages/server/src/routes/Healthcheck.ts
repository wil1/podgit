import StatusCodes from 'http-status-codes';
import { Request, Response, Router } from 'express';

const { Client } = require('pg')

const router = Router();
const { BAD_REQUEST, CREATED, OK, INTERNAL_SERVER_ERROR } = StatusCodes;

router.get('/', async (req: Request, res: Response) => {
    const client = new Client({
        user: 'gitpod',
        host: '127.0.0.1',
        database: 'postgres',
        //   password: 'secretpassword',
        port: 5432,
    })
    await client.connect()
    try {
        const { rows } = await client.query('SELECT 1 as healthcheck')
        if (rows[0]?.healthcheck === 1) {
            return res.status(OK).json({status: `healthy`});
        }
    } finally {
        await client.end()
    }
    
    return res.status(INTERNAL_SERVER_ERROR).json({error: `healthcheck failed`})
    
});

export default router;
