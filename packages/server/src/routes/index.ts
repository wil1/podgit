import { Router } from 'express';
import HealthcheckRouter from './Healthcheck';

// Init router and path
const router = Router();

// Add sub-routes
router.use('/Healthcheck', HealthcheckRouter);

// Export the base-router
export default router;
