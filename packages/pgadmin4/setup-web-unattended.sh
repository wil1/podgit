#!/bin/bash
set -eux

eval $(gp env --export)

if [ -z "$CYTON_DEV_USER" ] ; then
    gp env CYTON_DEV_USER=cyton
fi

if [ -z "$CYTON_DEV_PASSWORD" ] ; then
    gp env CYTON_DEV_PASSWORD=$(date | md5sum | head -c 32)
fi

eval $(gp env --export)

export AUTOMATED=1
export PGADMIN_DEFAULT_EMAIL=$CYTON_DEV_USER@cyton.net
export PGADMIN_SETUP_EMAIL=$PGADMIN_DEFAULT_EMAIL
export PGADMIN_DEFAULT_PASSWORD=$CYTON_DEV_PASSWORD
export PGADMIN_SETUP_PASSWORD=$PGADMIN_DEFAULT_PASSWORD
export PGADMIN_CONFIG_DEFAULT_SERVER=0.0.0.0
export PGADMIN_LISTEN_ADDRESS=0.0.0.0
export PGADMIN_SERVER_JSON_FILE=/workspace/podgit/packages/pgadmin4/servers.json
export PGADMIN_CONFIG_WTF_CSRF_ENABLED=False
export PGADMIN_CONFIG_WTF_CSRF_CHECK_DEFAULT=False

/home/gitpod/.pyenv/shims/pgadmin4 &

# wait for pgadmin to start
while ! nc -z localhost 5050; do   
  sleep 1
done

/home/gitpod/.pyenv/shims/python \
    /home/gitpod/.pyenv/versions/$(python -V | cut -d' ' -f2)/lib/python3.8/site-packages/pgadmin4/setup.py  \
    --load-servers $PGADMIN_SERVER_JSON_FILE \
    --user $PGADMIN_DEFAULT_EMAIL

