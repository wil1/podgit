import React from 'react'

export type MenuItemType = {
    color: string
    icon: string
    title: string
    link: string
}
const mainMenu : MenuItemType[] = [
    { color: "blue",   icon: "fa-home",  title: "Home", link: "/home" },
    { color: "green",  icon: "fa-folder",  title: "Files", link: "/files" },
    { color: "indigo",   icon: "fa-users",   title: "Users", link: "/users" },
    { color: "pink", icon: "fa-sitemap", title: "Groups", link: "/groups"},
    { color: "yellow", icon: "fa-tasks",   title: "Tasks", link: "/tasks" },
    { color: "red",    icon: "fa-code",    title: "Devtools", link: "/devtools" }
]

export type DevtoolsMenuItemType = Pick<MenuItemType, "link" | "title">

const devtoolsMenu : DevtoolsMenuItemType[] = [
    { title: "Graphiql", link: "graphiql" },
    { title: "Generate", link: "generate" },
]

export function useMenuItems() {
    return {
        mainMenu, devtoolsMenu
    }
}

