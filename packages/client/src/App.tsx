import React from 'react'

import {
  BrowserRouter as Router,
  Link,
  Redirect,
  Route,
  useRouteMatch
} from 'react-router-dom'

import { useAuth } from './features/auth/useAuth'
import Navigation from './layout/Navigation'
import Home from './pages/Home'
import Menu from './layout/Menu'
import UsersPage from './pages/Users'
import GroupsPage from './pages/Groups'
import FilesPage from './pages/Files'

const fetcher = (url: string) => fetch(url).then((res) => res.json())


function App() {

    const {auth, login, logout} = useAuth()
    if (auth.token) {
        return (
            <div className="bg-gray-900 font-sans leading-normal tracking-normal mt-12">
                <Navigation />
                <div className="flex flex-col md:flex-row">
                    <Menu />

                    <Route exact path="/home">
                        <Home />
                    </Route>

                    <Route path="/users">
                        <UsersPage />
                    </Route>

                    <Route path="/groups">
                        <GroupsPage />
                    </Route>

                    <Route path="/files">
                        <FilesPage />
                    </Route>

                    <Route exact path="/">
                        <Redirect to="/home" />
                    </Route>
                </div>
            </div>
        )
    }
    
    return <Redirect to="/welcome" />
}

export default App;
