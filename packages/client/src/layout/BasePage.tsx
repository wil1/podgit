import React from 'react'

export type BasePageProps = {
    title: string
}
const BasePage: React.FC<BasePageProps> = props =>  {
    
    return (
        <div className="main-content flex-1 bg-gray-100 mt-12 md:mt-2 pb-24 md:pb-5">
            
            <div className="bg-gray-900 pt-3">
                <div className="rounded-tl-3xl bg-gradient-to-r from-blue-900 to-gray-900 p-4 shadow text-2xl text-white">
                <h3 className="font-bold pl-2">{props.title}</h3>
                </div>
            </div>
            
            <div className="flex flex-wrap">
                {props.children}
            </div>

        </div>
    )
}

export default BasePage
    