import gql from 'graphql-tag';
import * as Urql from 'urql';
export type Maybe<T> = T | null;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
export type Omit<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>;
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  /** The `JSON` scalar type represents JSON values as specified by [ECMA-404](http://www.ecma-international.org/publications/files/ECMA-ST/ECMA-404.pdf). */
  JSON: any;
  /**
   * A point in time as described by the [ISO
   * 8601](https://en.wikipedia.org/wiki/ISO_8601) standard. May or may not include a timezone.
   */
  Datetime: any;
  /** A location in a connection that can be used for resuming pagination. */
  Cursor: any;
  /** The `Upload` scalar type represents a file upload. */
  Upload: any;
};

/** The root query type which gives access points into the data universe. */
export type Query = Node & {
  __typename?: 'Query';
  /**
   * Exposes the root query type nested one level down. This is helpful for Relay 1
   * which can only query top level fields if they are in a particular form.
   */
  query: Query;
  /** The root query type must be a `Node` to work well with Relay 1 mutations. This just resolves to `query`. */
  nodeId: Scalars['ID'];
  /** Fetches an object given its globally unique `ID`. */
  node?: Maybe<Node>;
  /** Reads and enables pagination through a set of `FileRevision`. */
  fileRevisions?: Maybe<FileRevisionsConnection>;
  /** Reads and enables pagination through a set of `FileUpload`. */
  fileUploads?: Maybe<FileUploadsConnection>;
  /** Reads and enables pagination through a set of `File`. */
  files?: Maybe<FilesConnection>;
  /** Reads and enables pagination through a set of `Group`. */
  groups?: Maybe<GroupsConnection>;
  /** Reads and enables pagination through a set of `Subgroup`. */
  subgroups?: Maybe<SubgroupsConnection>;
  /** Reads and enables pagination through a set of `UserSession`. */
  userSessions?: Maybe<UserSessionsConnection>;
  /** Reads and enables pagination through a set of `User`. */
  users?: Maybe<UsersConnection>;
  fileRevision?: Maybe<FileRevision>;
  fileUpload?: Maybe<FileUpload>;
  file?: Maybe<File>;
  group?: Maybe<Group>;
  user?: Maybe<User>;
  userByExternalId?: Maybe<User>;
  /** Reads a single `FileRevision` using its globally unique `ID`. */
  fileRevisionByNodeId?: Maybe<FileRevision>;
  /** Reads a single `FileUpload` using its globally unique `ID`. */
  fileUploadByNodeId?: Maybe<FileUpload>;
  /** Reads a single `File` using its globally unique `ID`. */
  fileByNodeId?: Maybe<File>;
  /** Reads a single `Group` using its globally unique `ID`. */
  groupByNodeId?: Maybe<Group>;
  /** Reads a single `User` using its globally unique `ID`. */
  userByNodeId?: Maybe<User>;
};


/** The root query type which gives access points into the data universe. */
export type QueryNodeArgs = {
  nodeId: Scalars['ID'];
};


/** The root query type which gives access points into the data universe. */
export type QueryFileRevisionsArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  before?: Maybe<Scalars['Cursor']>;
  after?: Maybe<Scalars['Cursor']>;
  orderBy?: Maybe<Array<FileRevisionsOrderBy>>;
  condition?: Maybe<FileRevisionCondition>;
};


/** The root query type which gives access points into the data universe. */
export type QueryFileUploadsArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  before?: Maybe<Scalars['Cursor']>;
  after?: Maybe<Scalars['Cursor']>;
  orderBy?: Maybe<Array<FileUploadsOrderBy>>;
  condition?: Maybe<FileUploadCondition>;
};


/** The root query type which gives access points into the data universe. */
export type QueryFilesArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  before?: Maybe<Scalars['Cursor']>;
  after?: Maybe<Scalars['Cursor']>;
  orderBy?: Maybe<Array<FilesOrderBy>>;
  condition?: Maybe<FileCondition>;
};


/** The root query type which gives access points into the data universe. */
export type QueryGroupsArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  before?: Maybe<Scalars['Cursor']>;
  after?: Maybe<Scalars['Cursor']>;
  orderBy?: Maybe<Array<GroupsOrderBy>>;
  condition?: Maybe<GroupCondition>;
};


/** The root query type which gives access points into the data universe. */
export type QuerySubgroupsArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  before?: Maybe<Scalars['Cursor']>;
  after?: Maybe<Scalars['Cursor']>;
  orderBy?: Maybe<Array<SubgroupsOrderBy>>;
  condition?: Maybe<SubgroupCondition>;
};


/** The root query type which gives access points into the data universe. */
export type QueryUserSessionsArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  before?: Maybe<Scalars['Cursor']>;
  after?: Maybe<Scalars['Cursor']>;
  orderBy?: Maybe<Array<UserSessionsOrderBy>>;
  condition?: Maybe<UserSessionCondition>;
};


/** The root query type which gives access points into the data universe. */
export type QueryUsersArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  before?: Maybe<Scalars['Cursor']>;
  after?: Maybe<Scalars['Cursor']>;
  orderBy?: Maybe<Array<UsersOrderBy>>;
  condition?: Maybe<UserCondition>;
};


/** The root query type which gives access points into the data universe. */
export type QueryFileRevisionArgs = {
  id: Scalars['Int'];
};


/** The root query type which gives access points into the data universe. */
export type QueryFileUploadArgs = {
  id: Scalars['Int'];
};


/** The root query type which gives access points into the data universe. */
export type QueryFileArgs = {
  id: Scalars['Int'];
};


/** The root query type which gives access points into the data universe. */
export type QueryGroupArgs = {
  id: Scalars['Int'];
};


/** The root query type which gives access points into the data universe. */
export type QueryUserArgs = {
  id: Scalars['Int'];
};


/** The root query type which gives access points into the data universe. */
export type QueryUserByExternalIdArgs = {
  externalId: Scalars['String'];
};


/** The root query type which gives access points into the data universe. */
export type QueryFileRevisionByNodeIdArgs = {
  nodeId: Scalars['ID'];
};


/** The root query type which gives access points into the data universe. */
export type QueryFileUploadByNodeIdArgs = {
  nodeId: Scalars['ID'];
};


/** The root query type which gives access points into the data universe. */
export type QueryFileByNodeIdArgs = {
  nodeId: Scalars['ID'];
};


/** The root query type which gives access points into the data universe. */
export type QueryGroupByNodeIdArgs = {
  nodeId: Scalars['ID'];
};


/** The root query type which gives access points into the data universe. */
export type QueryUserByNodeIdArgs = {
  nodeId: Scalars['ID'];
};

/** An object with a globally unique `ID`. */
export type Node = {
  /** A globally unique identifier. Can be used in various places throughout the system to identify this single value. */
  nodeId: Scalars['ID'];
};

/** A connection to a list of `FileRevision` values. */
export type FileRevisionsConnection = {
  __typename?: 'FileRevisionsConnection';
  /** A list of `FileRevision` objects. */
  nodes: Array<Maybe<FileRevision>>;
  /** A list of edges which contains the `FileRevision` and cursor to aid in pagination. */
  edges: Array<FileRevisionsEdge>;
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
  /** The count of *all* `FileRevision` you could get from the connection. */
  totalCount: Scalars['Int'];
};

export type FileRevision = Node & {
  __typename?: 'FileRevision';
  /** A globally unique identifier. Can be used in various places throughout the system to identify this single value. */
  nodeId: Scalars['ID'];
  id: Scalars['Int'];
  fileId?: Maybe<Scalars['Int']>;
  name: Scalars['String'];
  sha256: Scalars['String'];
  fileMetadata: Scalars['JSON'];
  modifyTimestamp: Scalars['Datetime'];
  /** Reads a single `File` that is related to this `FileRevision`. */
  file?: Maybe<File>;
};



export type File = Node & {
  __typename?: 'File';
  /** A globally unique identifier. Can be used in various places throughout the system to identify this single value. */
  nodeId: Scalars['ID'];
  id: Scalars['Int'];
  name: Scalars['String'];
  sha256: Scalars['String'];
  fileMetadata: Scalars['JSON'];
  modifyTimestamp: Scalars['Datetime'];
  /** Reads and enables pagination through a set of `FileRevision`. */
  fileRevisions: FileRevisionsConnection;
};


export type FileFileRevisionsArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  before?: Maybe<Scalars['Cursor']>;
  after?: Maybe<Scalars['Cursor']>;
  orderBy?: Maybe<Array<FileRevisionsOrderBy>>;
  condition?: Maybe<FileRevisionCondition>;
};


/** Methods to use when ordering `FileRevision`. */
export enum FileRevisionsOrderBy {
  Natural = 'NATURAL',
  IdAsc = 'ID_ASC',
  IdDesc = 'ID_DESC',
  FileIdAsc = 'FILE_ID_ASC',
  FileIdDesc = 'FILE_ID_DESC',
  NameAsc = 'NAME_ASC',
  NameDesc = 'NAME_DESC',
  Sha256Asc = 'SHA256_ASC',
  Sha256Desc = 'SHA256_DESC',
  FileMetadataAsc = 'FILE_METADATA_ASC',
  FileMetadataDesc = 'FILE_METADATA_DESC',
  ModifyTimestampAsc = 'MODIFY_TIMESTAMP_ASC',
  ModifyTimestampDesc = 'MODIFY_TIMESTAMP_DESC',
  PrimaryKeyAsc = 'PRIMARY_KEY_ASC',
  PrimaryKeyDesc = 'PRIMARY_KEY_DESC'
}

/**
 * A condition to be used against `FileRevision` object types. All fields are
 * tested for equality and combined with a logical ‘and.’
 */
export type FileRevisionCondition = {
  /** Checks for equality with the object’s `id` field. */
  id?: Maybe<Scalars['Int']>;
  /** Checks for equality with the object’s `fileId` field. */
  fileId?: Maybe<Scalars['Int']>;
  /** Checks for equality with the object’s `name` field. */
  name?: Maybe<Scalars['String']>;
  /** Checks for equality with the object’s `sha256` field. */
  sha256?: Maybe<Scalars['String']>;
  /** Checks for equality with the object’s `fileMetadata` field. */
  fileMetadata?: Maybe<Scalars['JSON']>;
  /** Checks for equality with the object’s `modifyTimestamp` field. */
  modifyTimestamp?: Maybe<Scalars['Datetime']>;
};

/** A `FileRevision` edge in the connection. */
export type FileRevisionsEdge = {
  __typename?: 'FileRevisionsEdge';
  /** A cursor for use in pagination. */
  cursor?: Maybe<Scalars['Cursor']>;
  /** The `FileRevision` at the end of the edge. */
  node?: Maybe<FileRevision>;
};

/** Information about pagination in a connection. */
export type PageInfo = {
  __typename?: 'PageInfo';
  /** When paginating forwards, are there more items? */
  hasNextPage: Scalars['Boolean'];
  /** When paginating backwards, are there more items? */
  hasPreviousPage: Scalars['Boolean'];
  /** When paginating backwards, the cursor to continue. */
  startCursor?: Maybe<Scalars['Cursor']>;
  /** When paginating forwards, the cursor to continue. */
  endCursor?: Maybe<Scalars['Cursor']>;
};

/** A connection to a list of `FileUpload` values. */
export type FileUploadsConnection = {
  __typename?: 'FileUploadsConnection';
  /** A list of `FileUpload` objects. */
  nodes: Array<Maybe<FileUpload>>;
  /** A list of edges which contains the `FileUpload` and cursor to aid in pagination. */
  edges: Array<FileUploadsEdge>;
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
  /** The count of *all* `FileUpload` you could get from the connection. */
  totalCount: Scalars['Int'];
};

export type FileUpload = Node & {
  __typename?: 'FileUpload';
  /** A globally unique identifier. Can be used in various places throughout the system to identify this single value. */
  nodeId: Scalars['ID'];
  id: Scalars['Int'];
  uploadFile?: Maybe<Scalars['JSON']>;
  uploadTimestamp: Scalars['Datetime'];
};

/** A `FileUpload` edge in the connection. */
export type FileUploadsEdge = {
  __typename?: 'FileUploadsEdge';
  /** A cursor for use in pagination. */
  cursor?: Maybe<Scalars['Cursor']>;
  /** The `FileUpload` at the end of the edge. */
  node?: Maybe<FileUpload>;
};

/** Methods to use when ordering `FileUpload`. */
export enum FileUploadsOrderBy {
  Natural = 'NATURAL',
  IdAsc = 'ID_ASC',
  IdDesc = 'ID_DESC',
  UploadFileAsc = 'UPLOAD_FILE_ASC',
  UploadFileDesc = 'UPLOAD_FILE_DESC',
  UploadTimestampAsc = 'UPLOAD_TIMESTAMP_ASC',
  UploadTimestampDesc = 'UPLOAD_TIMESTAMP_DESC',
  PrimaryKeyAsc = 'PRIMARY_KEY_ASC',
  PrimaryKeyDesc = 'PRIMARY_KEY_DESC'
}

/**
 * A condition to be used against `FileUpload` object types. All fields are tested
 * for equality and combined with a logical ‘and.’
 */
export type FileUploadCondition = {
  /** Checks for equality with the object’s `id` field. */
  id?: Maybe<Scalars['Int']>;
  /** Checks for equality with the object’s `uploadFile` field. */
  uploadFile?: Maybe<Scalars['JSON']>;
  /** Checks for equality with the object’s `uploadTimestamp` field. */
  uploadTimestamp?: Maybe<Scalars['Datetime']>;
};

/** A connection to a list of `File` values. */
export type FilesConnection = {
  __typename?: 'FilesConnection';
  /** A list of `File` objects. */
  nodes: Array<Maybe<File>>;
  /** A list of edges which contains the `File` and cursor to aid in pagination. */
  edges: Array<FilesEdge>;
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
  /** The count of *all* `File` you could get from the connection. */
  totalCount: Scalars['Int'];
};

/** A `File` edge in the connection. */
export type FilesEdge = {
  __typename?: 'FilesEdge';
  /** A cursor for use in pagination. */
  cursor?: Maybe<Scalars['Cursor']>;
  /** The `File` at the end of the edge. */
  node?: Maybe<File>;
};

/** Methods to use when ordering `File`. */
export enum FilesOrderBy {
  Natural = 'NATURAL',
  IdAsc = 'ID_ASC',
  IdDesc = 'ID_DESC',
  NameAsc = 'NAME_ASC',
  NameDesc = 'NAME_DESC',
  Sha256Asc = 'SHA256_ASC',
  Sha256Desc = 'SHA256_DESC',
  FileMetadataAsc = 'FILE_METADATA_ASC',
  FileMetadataDesc = 'FILE_METADATA_DESC',
  ModifyTimestampAsc = 'MODIFY_TIMESTAMP_ASC',
  ModifyTimestampDesc = 'MODIFY_TIMESTAMP_DESC',
  PrimaryKeyAsc = 'PRIMARY_KEY_ASC',
  PrimaryKeyDesc = 'PRIMARY_KEY_DESC'
}

/** A condition to be used against `File` object types. All fields are tested for equality and combined with a logical ‘and.’ */
export type FileCondition = {
  /** Checks for equality with the object’s `id` field. */
  id?: Maybe<Scalars['Int']>;
  /** Checks for equality with the object’s `name` field. */
  name?: Maybe<Scalars['String']>;
  /** Checks for equality with the object’s `sha256` field. */
  sha256?: Maybe<Scalars['String']>;
  /** Checks for equality with the object’s `fileMetadata` field. */
  fileMetadata?: Maybe<Scalars['JSON']>;
  /** Checks for equality with the object’s `modifyTimestamp` field. */
  modifyTimestamp?: Maybe<Scalars['Datetime']>;
};

/** A connection to a list of `Group` values. */
export type GroupsConnection = {
  __typename?: 'GroupsConnection';
  /** A list of `Group` objects. */
  nodes: Array<Maybe<Group>>;
  /** A list of edges which contains the `Group` and cursor to aid in pagination. */
  edges: Array<GroupsEdge>;
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
  /** The count of *all* `Group` you could get from the connection. */
  totalCount: Scalars['Int'];
};

export type Group = Node & {
  __typename?: 'Group';
  /** A globally unique identifier. Can be used in various places throughout the system to identify this single value. */
  nodeId: Scalars['ID'];
  id: Scalars['Int'];
  name: Scalars['String'];
  owner?: Maybe<Scalars['String']>;
  /** Reads and enables pagination through a set of `Subgroup`. */
  subgroups: SubgroupsConnection;
  /** Reads and enables pagination through a set of `Subgroup`. */
  subgroupsBySubgroupId: SubgroupsConnection;
};


export type GroupSubgroupsArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  before?: Maybe<Scalars['Cursor']>;
  after?: Maybe<Scalars['Cursor']>;
  orderBy?: Maybe<Array<SubgroupsOrderBy>>;
  condition?: Maybe<SubgroupCondition>;
};


export type GroupSubgroupsBySubgroupIdArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  before?: Maybe<Scalars['Cursor']>;
  after?: Maybe<Scalars['Cursor']>;
  orderBy?: Maybe<Array<SubgroupsOrderBy>>;
  condition?: Maybe<SubgroupCondition>;
};

/** A connection to a list of `Subgroup` values. */
export type SubgroupsConnection = {
  __typename?: 'SubgroupsConnection';
  /** A list of `Subgroup` objects. */
  nodes: Array<Maybe<Subgroup>>;
  /** A list of edges which contains the `Subgroup` and cursor to aid in pagination. */
  edges: Array<SubgroupsEdge>;
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
  /** The count of *all* `Subgroup` you could get from the connection. */
  totalCount: Scalars['Int'];
};

export type Subgroup = {
  __typename?: 'Subgroup';
  groupId?: Maybe<Scalars['Int']>;
  subgroupId?: Maybe<Scalars['Int']>;
  owner?: Maybe<Scalars['String']>;
  /** Reads a single `Group` that is related to this `Subgroup`. */
  group?: Maybe<Group>;
  /** Reads a single `Group` that is related to this `Subgroup`. */
  subgroup?: Maybe<Group>;
};

/** A `Subgroup` edge in the connection. */
export type SubgroupsEdge = {
  __typename?: 'SubgroupsEdge';
  /** A cursor for use in pagination. */
  cursor?: Maybe<Scalars['Cursor']>;
  /** The `Subgroup` at the end of the edge. */
  node?: Maybe<Subgroup>;
};

/** Methods to use when ordering `Subgroup`. */
export enum SubgroupsOrderBy {
  Natural = 'NATURAL',
  GroupIdAsc = 'GROUP_ID_ASC',
  GroupIdDesc = 'GROUP_ID_DESC',
  SubgroupIdAsc = 'SUBGROUP_ID_ASC',
  SubgroupIdDesc = 'SUBGROUP_ID_DESC',
  OwnerAsc = 'OWNER_ASC',
  OwnerDesc = 'OWNER_DESC'
}

/**
 * A condition to be used against `Subgroup` object types. All fields are tested
 * for equality and combined with a logical ‘and.’
 */
export type SubgroupCondition = {
  /** Checks for equality with the object’s `groupId` field. */
  groupId?: Maybe<Scalars['Int']>;
  /** Checks for equality with the object’s `subgroupId` field. */
  subgroupId?: Maybe<Scalars['Int']>;
  /** Checks for equality with the object’s `owner` field. */
  owner?: Maybe<Scalars['String']>;
};

/** A `Group` edge in the connection. */
export type GroupsEdge = {
  __typename?: 'GroupsEdge';
  /** A cursor for use in pagination. */
  cursor?: Maybe<Scalars['Cursor']>;
  /** The `Group` at the end of the edge. */
  node?: Maybe<Group>;
};

/** Methods to use when ordering `Group`. */
export enum GroupsOrderBy {
  Natural = 'NATURAL',
  IdAsc = 'ID_ASC',
  IdDesc = 'ID_DESC',
  NameAsc = 'NAME_ASC',
  NameDesc = 'NAME_DESC',
  OwnerAsc = 'OWNER_ASC',
  OwnerDesc = 'OWNER_DESC',
  PrimaryKeyAsc = 'PRIMARY_KEY_ASC',
  PrimaryKeyDesc = 'PRIMARY_KEY_DESC'
}

/** A condition to be used against `Group` object types. All fields are tested for equality and combined with a logical ‘and.’ */
export type GroupCondition = {
  /** Checks for equality with the object’s `id` field. */
  id?: Maybe<Scalars['Int']>;
  /** Checks for equality with the object’s `name` field. */
  name?: Maybe<Scalars['String']>;
  /** Checks for equality with the object’s `owner` field. */
  owner?: Maybe<Scalars['String']>;
};

/** A connection to a list of `UserSession` values. */
export type UserSessionsConnection = {
  __typename?: 'UserSessionsConnection';
  /** A list of `UserSession` objects. */
  nodes: Array<Maybe<UserSession>>;
  /** A list of edges which contains the `UserSession` and cursor to aid in pagination. */
  edges: Array<UserSessionsEdge>;
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
  /** The count of *all* `UserSession` you could get from the connection. */
  totalCount: Scalars['Int'];
};

export type UserSession = {
  __typename?: 'UserSession';
  id?: Maybe<Scalars['Int']>;
  externalId?: Maybe<Scalars['String']>;
  jwt?: Maybe<Scalars['JSON']>;
  roles?: Maybe<Scalars['JSON']>;
  preferredUsername?: Maybe<Scalars['String']>;
  givenName?: Maybe<Scalars['String']>;
  familyName?: Maybe<Scalars['String']>;
  email?: Maybe<Scalars['String']>;
};

/** A `UserSession` edge in the connection. */
export type UserSessionsEdge = {
  __typename?: 'UserSessionsEdge';
  /** A cursor for use in pagination. */
  cursor?: Maybe<Scalars['Cursor']>;
  /** The `UserSession` at the end of the edge. */
  node?: Maybe<UserSession>;
};

/** Methods to use when ordering `UserSession`. */
export enum UserSessionsOrderBy {
  Natural = 'NATURAL',
  IdAsc = 'ID_ASC',
  IdDesc = 'ID_DESC',
  ExternalIdAsc = 'EXTERNAL_ID_ASC',
  ExternalIdDesc = 'EXTERNAL_ID_DESC',
  JwtAsc = 'JWT_ASC',
  JwtDesc = 'JWT_DESC',
  RolesAsc = 'ROLES_ASC',
  RolesDesc = 'ROLES_DESC',
  PreferredUsernameAsc = 'PREFERRED_USERNAME_ASC',
  PreferredUsernameDesc = 'PREFERRED_USERNAME_DESC',
  GivenNameAsc = 'GIVEN_NAME_ASC',
  GivenNameDesc = 'GIVEN_NAME_DESC',
  FamilyNameAsc = 'FAMILY_NAME_ASC',
  FamilyNameDesc = 'FAMILY_NAME_DESC',
  EmailAsc = 'EMAIL_ASC',
  EmailDesc = 'EMAIL_DESC'
}

/**
 * A condition to be used against `UserSession` object types. All fields are tested
 * for equality and combined with a logical ‘and.’
 */
export type UserSessionCondition = {
  /** Checks for equality with the object’s `id` field. */
  id?: Maybe<Scalars['Int']>;
  /** Checks for equality with the object’s `externalId` field. */
  externalId?: Maybe<Scalars['String']>;
  /** Checks for equality with the object’s `jwt` field. */
  jwt?: Maybe<Scalars['JSON']>;
  /** Checks for equality with the object’s `roles` field. */
  roles?: Maybe<Scalars['JSON']>;
  /** Checks for equality with the object’s `preferredUsername` field. */
  preferredUsername?: Maybe<Scalars['String']>;
  /** Checks for equality with the object’s `givenName` field. */
  givenName?: Maybe<Scalars['String']>;
  /** Checks for equality with the object’s `familyName` field. */
  familyName?: Maybe<Scalars['String']>;
  /** Checks for equality with the object’s `email` field. */
  email?: Maybe<Scalars['String']>;
};

/** A connection to a list of `User` values. */
export type UsersConnection = {
  __typename?: 'UsersConnection';
  /** A list of `User` objects. */
  nodes: Array<Maybe<User>>;
  /** A list of edges which contains the `User` and cursor to aid in pagination. */
  edges: Array<UsersEdge>;
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
  /** The count of *all* `User` you could get from the connection. */
  totalCount: Scalars['Int'];
};

export type User = Node & {
  __typename?: 'User';
  /** A globally unique identifier. Can be used in various places throughout the system to identify this single value. */
  nodeId: Scalars['ID'];
  id: Scalars['Int'];
  externalId?: Maybe<Scalars['String']>;
  preferredUsername?: Maybe<Scalars['String']>;
  email?: Maybe<Scalars['String']>;
  givenName?: Maybe<Scalars['String']>;
  familyName?: Maybe<Scalars['String']>;
  roles?: Maybe<Scalars['JSON']>;
  lastLogin?: Maybe<Scalars['Datetime']>;
};

/** A `User` edge in the connection. */
export type UsersEdge = {
  __typename?: 'UsersEdge';
  /** A cursor for use in pagination. */
  cursor?: Maybe<Scalars['Cursor']>;
  /** The `User` at the end of the edge. */
  node?: Maybe<User>;
};

/** Methods to use when ordering `User`. */
export enum UsersOrderBy {
  Natural = 'NATURAL',
  IdAsc = 'ID_ASC',
  IdDesc = 'ID_DESC',
  ExternalIdAsc = 'EXTERNAL_ID_ASC',
  ExternalIdDesc = 'EXTERNAL_ID_DESC',
  PreferredUsernameAsc = 'PREFERRED_USERNAME_ASC',
  PreferredUsernameDesc = 'PREFERRED_USERNAME_DESC',
  EmailAsc = 'EMAIL_ASC',
  EmailDesc = 'EMAIL_DESC',
  GivenNameAsc = 'GIVEN_NAME_ASC',
  GivenNameDesc = 'GIVEN_NAME_DESC',
  FamilyNameAsc = 'FAMILY_NAME_ASC',
  FamilyNameDesc = 'FAMILY_NAME_DESC',
  RolesAsc = 'ROLES_ASC',
  RolesDesc = 'ROLES_DESC',
  LastLoginAsc = 'LAST_LOGIN_ASC',
  LastLoginDesc = 'LAST_LOGIN_DESC',
  PrimaryKeyAsc = 'PRIMARY_KEY_ASC',
  PrimaryKeyDesc = 'PRIMARY_KEY_DESC'
}

/** A condition to be used against `User` object types. All fields are tested for equality and combined with a logical ‘and.’ */
export type UserCondition = {
  /** Checks for equality with the object’s `id` field. */
  id?: Maybe<Scalars['Int']>;
  /** Checks for equality with the object’s `externalId` field. */
  externalId?: Maybe<Scalars['String']>;
  /** Checks for equality with the object’s `preferredUsername` field. */
  preferredUsername?: Maybe<Scalars['String']>;
  /** Checks for equality with the object’s `email` field. */
  email?: Maybe<Scalars['String']>;
  /** Checks for equality with the object’s `givenName` field. */
  givenName?: Maybe<Scalars['String']>;
  /** Checks for equality with the object’s `familyName` field. */
  familyName?: Maybe<Scalars['String']>;
  /** Checks for equality with the object’s `roles` field. */
  roles?: Maybe<Scalars['JSON']>;
  /** Checks for equality with the object’s `lastLogin` field. */
  lastLogin?: Maybe<Scalars['Datetime']>;
};

/** The root mutation type which contains root level fields which mutate data. */
export type Mutation = {
  __typename?: 'Mutation';
  /** Creates a single `FileUpload`. */
  createFileUpload?: Maybe<CreateFileUploadPayload>;
  /** Deletes a single `FileUpload` using its globally unique id. */
  deleteFileUploadByNodeId?: Maybe<DeleteFileUploadPayload>;
  /** Deletes a single `FileUpload` using a unique key. */
  deleteFileUpload?: Maybe<DeleteFileUploadPayload>;
  registerUser?: Maybe<RegisterUserPayload>;
  upsertGroup?: Maybe<UpsertGroupPayload>;
  importUploadFile?: Maybe<ImportUploadFilePayload>;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationCreateFileUploadArgs = {
  input: CreateFileUploadInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationDeleteFileUploadByNodeIdArgs = {
  input: DeleteFileUploadByNodeIdInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationDeleteFileUploadArgs = {
  input: DeleteFileUploadInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationRegisterUserArgs = {
  input: RegisterUserInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationUpsertGroupArgs = {
  input: UpsertGroupInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationImportUploadFileArgs = {
  input: ImportUploadFileInput;
};

/** The output of our create `FileUpload` mutation. */
export type CreateFileUploadPayload = {
  __typename?: 'CreateFileUploadPayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  /** The `FileUpload` that was created by this mutation. */
  fileUpload?: Maybe<FileUpload>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
  /** An edge for our `FileUpload`. May be used by Relay 1. */
  fileUploadEdge?: Maybe<FileUploadsEdge>;
};


/** The output of our create `FileUpload` mutation. */
export type CreateFileUploadPayloadFileUploadEdgeArgs = {
  orderBy?: Maybe<Array<FileUploadsOrderBy>>;
};

/** All input for the create `FileUpload` mutation. */
export type CreateFileUploadInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  /** The `FileUpload` to be created by this mutation. */
  fileUpload: FileUploadInput;
};

/** An input for mutations affecting `FileUpload` */
export type FileUploadInput = {
  id?: Maybe<Scalars['Int']>;
  uploadFile?: Maybe<Scalars['Upload']>;
  uploadTimestamp?: Maybe<Scalars['Datetime']>;
};


/** The output of our delete `FileUpload` mutation. */
export type DeleteFileUploadPayload = {
  __typename?: 'DeleteFileUploadPayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  /** The `FileUpload` that was deleted by this mutation. */
  fileUpload?: Maybe<FileUpload>;
  deletedFileUploadNodeId?: Maybe<Scalars['ID']>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
  /** An edge for our `FileUpload`. May be used by Relay 1. */
  fileUploadEdge?: Maybe<FileUploadsEdge>;
};


/** The output of our delete `FileUpload` mutation. */
export type DeleteFileUploadPayloadFileUploadEdgeArgs = {
  orderBy?: Maybe<Array<FileUploadsOrderBy>>;
};

/** All input for the `deleteFileUploadByNodeId` mutation. */
export type DeleteFileUploadByNodeIdInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  /** The globally unique `ID` which will identify a single `FileUpload` to be deleted. */
  nodeId: Scalars['ID'];
};

/** All input for the `deleteFileUpload` mutation. */
export type DeleteFileUploadInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  id: Scalars['Int'];
};

/** The output of our `registerUser` mutation. */
export type RegisterUserPayload = {
  __typename?: 'RegisterUserPayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  userSessions?: Maybe<Array<Maybe<UserSession>>>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
};

/** All input for the `registerUser` mutation. */
export type RegisterUserInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: Maybe<Scalars['String']>;
};

/** The output of our `upsertGroup` mutation. */
export type UpsertGroupPayload = {
  __typename?: 'UpsertGroupPayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  group?: Maybe<Group>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
  /** An edge for our `Group`. May be used by Relay 1. */
  groupEdge?: Maybe<GroupsEdge>;
};


/** The output of our `upsertGroup` mutation. */
export type UpsertGroupPayloadGroupEdgeArgs = {
  orderBy?: Maybe<Array<GroupsOrderBy>>;
};

/** All input for the `upsertGroup` mutation. */
export type UpsertGroupInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  groupInput?: Maybe<GroupInput>;
};

/** An input for mutations affecting `Group` */
export type GroupInput = {
  id?: Maybe<Scalars['Int']>;
  name: Scalars['String'];
  owner?: Maybe<Scalars['String']>;
};

export type ImportUploadFilePayload = {
  __typename?: 'ImportUploadFilePayload';
  fileId: Scalars['Int'];
  query?: Maybe<Query>;
};

export type ImportUploadFileInput = {
  uploadFileId: Scalars['Int'];
  fileId?: Maybe<Scalars['Int']>;
};

/**
 * The root subscription type: contains events and live queries you can subscribe to with the `subscription` operation.
 * 
 * #### Live Queries
 * 
 * Live query fields are differentiated by containing `(live)` at the end of their
 * description, they are added for each field in the `Query` type. When you
 * subscribe to a live query field, the selection set will be evaluated and sent to
 * the client, and then most things\* that would cause the output of the selection
 * set to change will trigger the selection set to be re-evaluated and the results
 * to be re-sent to the client.
 * 
 * _(\* Not everything: typically only changes to persisted data referenced by the query are detected, not computed fields.)_
 * 
 * Live queries can be very expensive, so try and keep them small and focussed.
 * 
 * #### Events
 * 
 * Event fields will run their selection set when, and only when, the specified
 * server-side event occurs. This makes them a lot more efficient than Live
 * Queries, but it is still recommended that you keep payloads fairly small.
 */
export type Subscription = {
  __typename?: 'Subscription';
  /**
   * Exposes the root query type nested one level down. This is helpful for Relay 1
   * which can only query top level fields if they are in a particular form. (live)
   */
  query: Query;
  /** The root query type must be a `Node` to work well with Relay 1 mutations. This just resolves to `query`. (live) */
  nodeId: Scalars['ID'];
  /** Fetches an object given its globally unique `ID`. (live) */
  node?: Maybe<Node>;
  /** Reads and enables pagination through a set of `FileRevision`. (live) */
  fileRevisions?: Maybe<FileRevisionsConnection>;
  /** Reads and enables pagination through a set of `FileUpload`. (live) */
  fileUploads?: Maybe<FileUploadsConnection>;
  /** Reads and enables pagination through a set of `File`. (live) */
  files?: Maybe<FilesConnection>;
  /** Reads and enables pagination through a set of `Group`. (live) */
  groups?: Maybe<GroupsConnection>;
  /** Reads and enables pagination through a set of `Subgroup`. (live) */
  subgroups?: Maybe<SubgroupsConnection>;
  /** Reads and enables pagination through a set of `UserSession`. (live) */
  userSessions?: Maybe<UserSessionsConnection>;
  /** Reads and enables pagination through a set of `User`. (live) */
  users?: Maybe<UsersConnection>;
  /**  (live) */
  fileRevision?: Maybe<FileRevision>;
  /**  (live) */
  fileUpload?: Maybe<FileUpload>;
  /**  (live) */
  file?: Maybe<File>;
  /**  (live) */
  group?: Maybe<Group>;
  /**  (live) */
  user?: Maybe<User>;
  /**  (live) */
  userByExternalId?: Maybe<User>;
  /** Reads a single `FileRevision` using its globally unique `ID`. (live) */
  fileRevisionByNodeId?: Maybe<FileRevision>;
  /** Reads a single `FileUpload` using its globally unique `ID`. (live) */
  fileUploadByNodeId?: Maybe<FileUpload>;
  /** Reads a single `File` using its globally unique `ID`. (live) */
  fileByNodeId?: Maybe<File>;
  /** Reads a single `Group` using its globally unique `ID`. (live) */
  groupByNodeId?: Maybe<Group>;
  /** Reads a single `User` using its globally unique `ID`. (live) */
  userByNodeId?: Maybe<User>;
  listen: ListenPayload;
};


/**
 * The root subscription type: contains events and live queries you can subscribe to with the `subscription` operation.
 * 
 * #### Live Queries
 * 
 * Live query fields are differentiated by containing `(live)` at the end of their
 * description, they are added for each field in the `Query` type. When you
 * subscribe to a live query field, the selection set will be evaluated and sent to
 * the client, and then most things\* that would cause the output of the selection
 * set to change will trigger the selection set to be re-evaluated and the results
 * to be re-sent to the client.
 * 
 * _(\* Not everything: typically only changes to persisted data referenced by the query are detected, not computed fields.)_
 * 
 * Live queries can be very expensive, so try and keep them small and focussed.
 * 
 * #### Events
 * 
 * Event fields will run their selection set when, and only when, the specified
 * server-side event occurs. This makes them a lot more efficient than Live
 * Queries, but it is still recommended that you keep payloads fairly small.
 */
export type SubscriptionNodeArgs = {
  nodeId: Scalars['ID'];
};


/**
 * The root subscription type: contains events and live queries you can subscribe to with the `subscription` operation.
 * 
 * #### Live Queries
 * 
 * Live query fields are differentiated by containing `(live)` at the end of their
 * description, they are added for each field in the `Query` type. When you
 * subscribe to a live query field, the selection set will be evaluated and sent to
 * the client, and then most things\* that would cause the output of the selection
 * set to change will trigger the selection set to be re-evaluated and the results
 * to be re-sent to the client.
 * 
 * _(\* Not everything: typically only changes to persisted data referenced by the query are detected, not computed fields.)_
 * 
 * Live queries can be very expensive, so try and keep them small and focussed.
 * 
 * #### Events
 * 
 * Event fields will run their selection set when, and only when, the specified
 * server-side event occurs. This makes them a lot more efficient than Live
 * Queries, but it is still recommended that you keep payloads fairly small.
 */
export type SubscriptionFileRevisionsArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  before?: Maybe<Scalars['Cursor']>;
  after?: Maybe<Scalars['Cursor']>;
  orderBy?: Maybe<Array<FileRevisionsOrderBy>>;
  condition?: Maybe<FileRevisionCondition>;
};


/**
 * The root subscription type: contains events and live queries you can subscribe to with the `subscription` operation.
 * 
 * #### Live Queries
 * 
 * Live query fields are differentiated by containing `(live)` at the end of their
 * description, they are added for each field in the `Query` type. When you
 * subscribe to a live query field, the selection set will be evaluated and sent to
 * the client, and then most things\* that would cause the output of the selection
 * set to change will trigger the selection set to be re-evaluated and the results
 * to be re-sent to the client.
 * 
 * _(\* Not everything: typically only changes to persisted data referenced by the query are detected, not computed fields.)_
 * 
 * Live queries can be very expensive, so try and keep them small and focussed.
 * 
 * #### Events
 * 
 * Event fields will run their selection set when, and only when, the specified
 * server-side event occurs. This makes them a lot more efficient than Live
 * Queries, but it is still recommended that you keep payloads fairly small.
 */
export type SubscriptionFileUploadsArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  before?: Maybe<Scalars['Cursor']>;
  after?: Maybe<Scalars['Cursor']>;
  orderBy?: Maybe<Array<FileUploadsOrderBy>>;
  condition?: Maybe<FileUploadCondition>;
};


/**
 * The root subscription type: contains events and live queries you can subscribe to with the `subscription` operation.
 * 
 * #### Live Queries
 * 
 * Live query fields are differentiated by containing `(live)` at the end of their
 * description, they are added for each field in the `Query` type. When you
 * subscribe to a live query field, the selection set will be evaluated and sent to
 * the client, and then most things\* that would cause the output of the selection
 * set to change will trigger the selection set to be re-evaluated and the results
 * to be re-sent to the client.
 * 
 * _(\* Not everything: typically only changes to persisted data referenced by the query are detected, not computed fields.)_
 * 
 * Live queries can be very expensive, so try and keep them small and focussed.
 * 
 * #### Events
 * 
 * Event fields will run their selection set when, and only when, the specified
 * server-side event occurs. This makes them a lot more efficient than Live
 * Queries, but it is still recommended that you keep payloads fairly small.
 */
export type SubscriptionFilesArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  before?: Maybe<Scalars['Cursor']>;
  after?: Maybe<Scalars['Cursor']>;
  orderBy?: Maybe<Array<FilesOrderBy>>;
  condition?: Maybe<FileCondition>;
};


/**
 * The root subscription type: contains events and live queries you can subscribe to with the `subscription` operation.
 * 
 * #### Live Queries
 * 
 * Live query fields are differentiated by containing `(live)` at the end of their
 * description, they are added for each field in the `Query` type. When you
 * subscribe to a live query field, the selection set will be evaluated and sent to
 * the client, and then most things\* that would cause the output of the selection
 * set to change will trigger the selection set to be re-evaluated and the results
 * to be re-sent to the client.
 * 
 * _(\* Not everything: typically only changes to persisted data referenced by the query are detected, not computed fields.)_
 * 
 * Live queries can be very expensive, so try and keep them small and focussed.
 * 
 * #### Events
 * 
 * Event fields will run their selection set when, and only when, the specified
 * server-side event occurs. This makes them a lot more efficient than Live
 * Queries, but it is still recommended that you keep payloads fairly small.
 */
export type SubscriptionGroupsArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  before?: Maybe<Scalars['Cursor']>;
  after?: Maybe<Scalars['Cursor']>;
  orderBy?: Maybe<Array<GroupsOrderBy>>;
  condition?: Maybe<GroupCondition>;
};


/**
 * The root subscription type: contains events and live queries you can subscribe to with the `subscription` operation.
 * 
 * #### Live Queries
 * 
 * Live query fields are differentiated by containing `(live)` at the end of their
 * description, they are added for each field in the `Query` type. When you
 * subscribe to a live query field, the selection set will be evaluated and sent to
 * the client, and then most things\* that would cause the output of the selection
 * set to change will trigger the selection set to be re-evaluated and the results
 * to be re-sent to the client.
 * 
 * _(\* Not everything: typically only changes to persisted data referenced by the query are detected, not computed fields.)_
 * 
 * Live queries can be very expensive, so try and keep them small and focussed.
 * 
 * #### Events
 * 
 * Event fields will run their selection set when, and only when, the specified
 * server-side event occurs. This makes them a lot more efficient than Live
 * Queries, but it is still recommended that you keep payloads fairly small.
 */
export type SubscriptionSubgroupsArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  before?: Maybe<Scalars['Cursor']>;
  after?: Maybe<Scalars['Cursor']>;
  orderBy?: Maybe<Array<SubgroupsOrderBy>>;
  condition?: Maybe<SubgroupCondition>;
};


/**
 * The root subscription type: contains events and live queries you can subscribe to with the `subscription` operation.
 * 
 * #### Live Queries
 * 
 * Live query fields are differentiated by containing `(live)` at the end of their
 * description, they are added for each field in the `Query` type. When you
 * subscribe to a live query field, the selection set will be evaluated and sent to
 * the client, and then most things\* that would cause the output of the selection
 * set to change will trigger the selection set to be re-evaluated and the results
 * to be re-sent to the client.
 * 
 * _(\* Not everything: typically only changes to persisted data referenced by the query are detected, not computed fields.)_
 * 
 * Live queries can be very expensive, so try and keep them small and focussed.
 * 
 * #### Events
 * 
 * Event fields will run their selection set when, and only when, the specified
 * server-side event occurs. This makes them a lot more efficient than Live
 * Queries, but it is still recommended that you keep payloads fairly small.
 */
export type SubscriptionUserSessionsArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  before?: Maybe<Scalars['Cursor']>;
  after?: Maybe<Scalars['Cursor']>;
  orderBy?: Maybe<Array<UserSessionsOrderBy>>;
  condition?: Maybe<UserSessionCondition>;
};


/**
 * The root subscription type: contains events and live queries you can subscribe to with the `subscription` operation.
 * 
 * #### Live Queries
 * 
 * Live query fields are differentiated by containing `(live)` at the end of their
 * description, they are added for each field in the `Query` type. When you
 * subscribe to a live query field, the selection set will be evaluated and sent to
 * the client, and then most things\* that would cause the output of the selection
 * set to change will trigger the selection set to be re-evaluated and the results
 * to be re-sent to the client.
 * 
 * _(\* Not everything: typically only changes to persisted data referenced by the query are detected, not computed fields.)_
 * 
 * Live queries can be very expensive, so try and keep them small and focussed.
 * 
 * #### Events
 * 
 * Event fields will run their selection set when, and only when, the specified
 * server-side event occurs. This makes them a lot more efficient than Live
 * Queries, but it is still recommended that you keep payloads fairly small.
 */
export type SubscriptionUsersArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  before?: Maybe<Scalars['Cursor']>;
  after?: Maybe<Scalars['Cursor']>;
  orderBy?: Maybe<Array<UsersOrderBy>>;
  condition?: Maybe<UserCondition>;
};


/**
 * The root subscription type: contains events and live queries you can subscribe to with the `subscription` operation.
 * 
 * #### Live Queries
 * 
 * Live query fields are differentiated by containing `(live)` at the end of their
 * description, they are added for each field in the `Query` type. When you
 * subscribe to a live query field, the selection set will be evaluated and sent to
 * the client, and then most things\* that would cause the output of the selection
 * set to change will trigger the selection set to be re-evaluated and the results
 * to be re-sent to the client.
 * 
 * _(\* Not everything: typically only changes to persisted data referenced by the query are detected, not computed fields.)_
 * 
 * Live queries can be very expensive, so try and keep them small and focussed.
 * 
 * #### Events
 * 
 * Event fields will run their selection set when, and only when, the specified
 * server-side event occurs. This makes them a lot more efficient than Live
 * Queries, but it is still recommended that you keep payloads fairly small.
 */
export type SubscriptionFileRevisionArgs = {
  id: Scalars['Int'];
};


/**
 * The root subscription type: contains events and live queries you can subscribe to with the `subscription` operation.
 * 
 * #### Live Queries
 * 
 * Live query fields are differentiated by containing `(live)` at the end of their
 * description, they are added for each field in the `Query` type. When you
 * subscribe to a live query field, the selection set will be evaluated and sent to
 * the client, and then most things\* that would cause the output of the selection
 * set to change will trigger the selection set to be re-evaluated and the results
 * to be re-sent to the client.
 * 
 * _(\* Not everything: typically only changes to persisted data referenced by the query are detected, not computed fields.)_
 * 
 * Live queries can be very expensive, so try and keep them small and focussed.
 * 
 * #### Events
 * 
 * Event fields will run their selection set when, and only when, the specified
 * server-side event occurs. This makes them a lot more efficient than Live
 * Queries, but it is still recommended that you keep payloads fairly small.
 */
export type SubscriptionFileUploadArgs = {
  id: Scalars['Int'];
};


/**
 * The root subscription type: contains events and live queries you can subscribe to with the `subscription` operation.
 * 
 * #### Live Queries
 * 
 * Live query fields are differentiated by containing `(live)` at the end of their
 * description, they are added for each field in the `Query` type. When you
 * subscribe to a live query field, the selection set will be evaluated and sent to
 * the client, and then most things\* that would cause the output of the selection
 * set to change will trigger the selection set to be re-evaluated and the results
 * to be re-sent to the client.
 * 
 * _(\* Not everything: typically only changes to persisted data referenced by the query are detected, not computed fields.)_
 * 
 * Live queries can be very expensive, so try and keep them small and focussed.
 * 
 * #### Events
 * 
 * Event fields will run their selection set when, and only when, the specified
 * server-side event occurs. This makes them a lot more efficient than Live
 * Queries, but it is still recommended that you keep payloads fairly small.
 */
export type SubscriptionFileArgs = {
  id: Scalars['Int'];
};


/**
 * The root subscription type: contains events and live queries you can subscribe to with the `subscription` operation.
 * 
 * #### Live Queries
 * 
 * Live query fields are differentiated by containing `(live)` at the end of their
 * description, they are added for each field in the `Query` type. When you
 * subscribe to a live query field, the selection set will be evaluated and sent to
 * the client, and then most things\* that would cause the output of the selection
 * set to change will trigger the selection set to be re-evaluated and the results
 * to be re-sent to the client.
 * 
 * _(\* Not everything: typically only changes to persisted data referenced by the query are detected, not computed fields.)_
 * 
 * Live queries can be very expensive, so try and keep them small and focussed.
 * 
 * #### Events
 * 
 * Event fields will run their selection set when, and only when, the specified
 * server-side event occurs. This makes them a lot more efficient than Live
 * Queries, but it is still recommended that you keep payloads fairly small.
 */
export type SubscriptionGroupArgs = {
  id: Scalars['Int'];
};


/**
 * The root subscription type: contains events and live queries you can subscribe to with the `subscription` operation.
 * 
 * #### Live Queries
 * 
 * Live query fields are differentiated by containing `(live)` at the end of their
 * description, they are added for each field in the `Query` type. When you
 * subscribe to a live query field, the selection set will be evaluated and sent to
 * the client, and then most things\* that would cause the output of the selection
 * set to change will trigger the selection set to be re-evaluated and the results
 * to be re-sent to the client.
 * 
 * _(\* Not everything: typically only changes to persisted data referenced by the query are detected, not computed fields.)_
 * 
 * Live queries can be very expensive, so try and keep them small and focussed.
 * 
 * #### Events
 * 
 * Event fields will run their selection set when, and only when, the specified
 * server-side event occurs. This makes them a lot more efficient than Live
 * Queries, but it is still recommended that you keep payloads fairly small.
 */
export type SubscriptionUserArgs = {
  id: Scalars['Int'];
};


/**
 * The root subscription type: contains events and live queries you can subscribe to with the `subscription` operation.
 * 
 * #### Live Queries
 * 
 * Live query fields are differentiated by containing `(live)` at the end of their
 * description, they are added for each field in the `Query` type. When you
 * subscribe to a live query field, the selection set will be evaluated and sent to
 * the client, and then most things\* that would cause the output of the selection
 * set to change will trigger the selection set to be re-evaluated and the results
 * to be re-sent to the client.
 * 
 * _(\* Not everything: typically only changes to persisted data referenced by the query are detected, not computed fields.)_
 * 
 * Live queries can be very expensive, so try and keep them small and focussed.
 * 
 * #### Events
 * 
 * Event fields will run their selection set when, and only when, the specified
 * server-side event occurs. This makes them a lot more efficient than Live
 * Queries, but it is still recommended that you keep payloads fairly small.
 */
export type SubscriptionUserByExternalIdArgs = {
  externalId: Scalars['String'];
};


/**
 * The root subscription type: contains events and live queries you can subscribe to with the `subscription` operation.
 * 
 * #### Live Queries
 * 
 * Live query fields are differentiated by containing `(live)` at the end of their
 * description, they are added for each field in the `Query` type. When you
 * subscribe to a live query field, the selection set will be evaluated and sent to
 * the client, and then most things\* that would cause the output of the selection
 * set to change will trigger the selection set to be re-evaluated and the results
 * to be re-sent to the client.
 * 
 * _(\* Not everything: typically only changes to persisted data referenced by the query are detected, not computed fields.)_
 * 
 * Live queries can be very expensive, so try and keep them small and focussed.
 * 
 * #### Events
 * 
 * Event fields will run their selection set when, and only when, the specified
 * server-side event occurs. This makes them a lot more efficient than Live
 * Queries, but it is still recommended that you keep payloads fairly small.
 */
export type SubscriptionFileRevisionByNodeIdArgs = {
  nodeId: Scalars['ID'];
};


/**
 * The root subscription type: contains events and live queries you can subscribe to with the `subscription` operation.
 * 
 * #### Live Queries
 * 
 * Live query fields are differentiated by containing `(live)` at the end of their
 * description, they are added for each field in the `Query` type. When you
 * subscribe to a live query field, the selection set will be evaluated and sent to
 * the client, and then most things\* that would cause the output of the selection
 * set to change will trigger the selection set to be re-evaluated and the results
 * to be re-sent to the client.
 * 
 * _(\* Not everything: typically only changes to persisted data referenced by the query are detected, not computed fields.)_
 * 
 * Live queries can be very expensive, so try and keep them small and focussed.
 * 
 * #### Events
 * 
 * Event fields will run their selection set when, and only when, the specified
 * server-side event occurs. This makes them a lot more efficient than Live
 * Queries, but it is still recommended that you keep payloads fairly small.
 */
export type SubscriptionFileUploadByNodeIdArgs = {
  nodeId: Scalars['ID'];
};


/**
 * The root subscription type: contains events and live queries you can subscribe to with the `subscription` operation.
 * 
 * #### Live Queries
 * 
 * Live query fields are differentiated by containing `(live)` at the end of their
 * description, they are added for each field in the `Query` type. When you
 * subscribe to a live query field, the selection set will be evaluated and sent to
 * the client, and then most things\* that would cause the output of the selection
 * set to change will trigger the selection set to be re-evaluated and the results
 * to be re-sent to the client.
 * 
 * _(\* Not everything: typically only changes to persisted data referenced by the query are detected, not computed fields.)_
 * 
 * Live queries can be very expensive, so try and keep them small and focussed.
 * 
 * #### Events
 * 
 * Event fields will run their selection set when, and only when, the specified
 * server-side event occurs. This makes them a lot more efficient than Live
 * Queries, but it is still recommended that you keep payloads fairly small.
 */
export type SubscriptionFileByNodeIdArgs = {
  nodeId: Scalars['ID'];
};


/**
 * The root subscription type: contains events and live queries you can subscribe to with the `subscription` operation.
 * 
 * #### Live Queries
 * 
 * Live query fields are differentiated by containing `(live)` at the end of their
 * description, they are added for each field in the `Query` type. When you
 * subscribe to a live query field, the selection set will be evaluated and sent to
 * the client, and then most things\* that would cause the output of the selection
 * set to change will trigger the selection set to be re-evaluated and the results
 * to be re-sent to the client.
 * 
 * _(\* Not everything: typically only changes to persisted data referenced by the query are detected, not computed fields.)_
 * 
 * Live queries can be very expensive, so try and keep them small and focussed.
 * 
 * #### Events
 * 
 * Event fields will run their selection set when, and only when, the specified
 * server-side event occurs. This makes them a lot more efficient than Live
 * Queries, but it is still recommended that you keep payloads fairly small.
 */
export type SubscriptionGroupByNodeIdArgs = {
  nodeId: Scalars['ID'];
};


/**
 * The root subscription type: contains events and live queries you can subscribe to with the `subscription` operation.
 * 
 * #### Live Queries
 * 
 * Live query fields are differentiated by containing `(live)` at the end of their
 * description, they are added for each field in the `Query` type. When you
 * subscribe to a live query field, the selection set will be evaluated and sent to
 * the client, and then most things\* that would cause the output of the selection
 * set to change will trigger the selection set to be re-evaluated and the results
 * to be re-sent to the client.
 * 
 * _(\* Not everything: typically only changes to persisted data referenced by the query are detected, not computed fields.)_
 * 
 * Live queries can be very expensive, so try and keep them small and focussed.
 * 
 * #### Events
 * 
 * Event fields will run their selection set when, and only when, the specified
 * server-side event occurs. This makes them a lot more efficient than Live
 * Queries, but it is still recommended that you keep payloads fairly small.
 */
export type SubscriptionUserByNodeIdArgs = {
  nodeId: Scalars['ID'];
};


/**
 * The root subscription type: contains events and live queries you can subscribe to with the `subscription` operation.
 * 
 * #### Live Queries
 * 
 * Live query fields are differentiated by containing `(live)` at the end of their
 * description, they are added for each field in the `Query` type. When you
 * subscribe to a live query field, the selection set will be evaluated and sent to
 * the client, and then most things\* that would cause the output of the selection
 * set to change will trigger the selection set to be re-evaluated and the results
 * to be re-sent to the client.
 * 
 * _(\* Not everything: typically only changes to persisted data referenced by the query are detected, not computed fields.)_
 * 
 * Live queries can be very expensive, so try and keep them small and focussed.
 * 
 * #### Events
 * 
 * Event fields will run their selection set when, and only when, the specified
 * server-side event occurs. This makes them a lot more efficient than Live
 * Queries, but it is still recommended that you keep payloads fairly small.
 */
export type SubscriptionListenArgs = {
  topic: Scalars['String'];
};

export type ListenPayload = {
  __typename?: 'ListenPayload';
  /** Our root query field type. Allows us to run any query from our subscription payload. */
  query?: Maybe<Query>;
  relatedNode?: Maybe<Node>;
  relatedNodeId?: Maybe<Scalars['ID']>;
};

export type GroupListQueryVariables = Exact<{ [key: string]: never; }>;


export type GroupListQuery = (
  { __typename?: 'Query' }
  & { groups?: Maybe<(
    { __typename?: 'GroupsConnection' }
    & { nodes: Array<Maybe<(
      { __typename: 'Group' }
      & Pick<Group, 'id' | 'name' | 'owner' | 'nodeId'>
      & { subgroups: (
        { __typename?: 'SubgroupsConnection' }
        & { nodes: Array<Maybe<(
          { __typename?: 'Subgroup' }
          & Pick<Subgroup, 'owner'>
          & { subgroup?: Maybe<(
            { __typename: 'Group' }
            & Pick<Group, 'id' | 'name' | 'nodeId' | 'owner'>
          )> }
        )>> }
      ) }
    )>> }
  )> }
);

export type ImportUploadFileMutationVariables = Exact<{
  input: ImportUploadFileInput;
}>;


export type ImportUploadFileMutation = (
  { __typename?: 'Mutation' }
  & { importUploadFile?: Maybe<(
    { __typename?: 'ImportUploadFilePayload' }
    & Pick<ImportUploadFilePayload, 'fileId'>
  )> }
);

export type UploadFileMutationVariables = Exact<{
  input: CreateFileUploadInput;
}>;


export type UploadFileMutation = (
  { __typename?: 'Mutation' }
  & { createFileUpload?: Maybe<(
    { __typename?: 'CreateFileUploadPayload' }
    & { fileUpload?: Maybe<(
      { __typename: 'FileUpload' }
      & Pick<FileUpload, 'id' | 'uploadFile' | 'uploadTimestamp'>
    )> }
  )> }
);

export type UpsertGroupMutationVariables = Exact<{
  input: GroupInput;
}>;


export type UpsertGroupMutation = (
  { __typename?: 'Mutation' }
  & { upsertGroup?: Maybe<(
    { __typename?: 'UpsertGroupPayload' }
    & { group?: Maybe<(
      { __typename: 'Group' }
      & Pick<Group, 'id' | 'name' | 'owner' | 'nodeId'>
    )> }
  )> }
);

export type UserListQueryVariables = Exact<{ [key: string]: never; }>;


export type UserListQuery = (
  { __typename?: 'Query' }
  & { users?: Maybe<(
    { __typename?: 'UsersConnection' }
    & { nodes: Array<Maybe<(
      { __typename: 'User' }
      & Pick<User, 'givenName' | 'familyName' | 'preferredUsername' | 'email' | 'externalId' | 'id' | 'lastLogin' | 'roles' | 'nodeId'>
    )>> }
  )> }
);


export const GroupListDocument = gql`
    query GroupList {
  groups {
    nodes {
      __typename
      id
      name
      owner
      nodeId
      subgroups {
        nodes {
          subgroup {
            __typename
            id
            name
            nodeId
            owner
          }
          owner
        }
      }
    }
  }
}
    `;

export function useGroupListQuery(options: Omit<Urql.UseQueryArgs<GroupListQueryVariables>, 'query'> = {}) {
  return Urql.useQuery<GroupListQuery>({ query: GroupListDocument, ...options });
};
export const ImportUploadFileDocument = gql`
    mutation ImportUploadFile($input: ImportUploadFileInput!) {
  importUploadFile(input: $input) {
    fileId
  }
}
    `;

export function useImportUploadFileMutation() {
  return Urql.useMutation<ImportUploadFileMutation, ImportUploadFileMutationVariables>(ImportUploadFileDocument);
};
export const UploadFileDocument = gql`
    mutation UploadFile($input: CreateFileUploadInput!) {
  createFileUpload(input: $input) {
    fileUpload {
      __typename
      id
      uploadFile
      uploadTimestamp
    }
  }
}
    `;

export function useUploadFileMutation() {
  return Urql.useMutation<UploadFileMutation, UploadFileMutationVariables>(UploadFileDocument);
};
export const UpsertGroupDocument = gql`
    mutation UpsertGroup($input: GroupInput!) {
  upsertGroup(input: {groupInput: $input}) {
    group {
      __typename
      id
      name
      owner
      nodeId
    }
  }
}
    `;

export function useUpsertGroupMutation() {
  return Urql.useMutation<UpsertGroupMutation, UpsertGroupMutationVariables>(UpsertGroupDocument);
};
export const UserListDocument = gql`
    query UserList {
  users {
    nodes {
      __typename
      givenName
      familyName
      preferredUsername
      email
      externalId
      id
      lastLogin
      roles
      nodeId
    }
  }
}
    `;

export function useUserListQuery(options: Omit<Urql.UseQueryArgs<UserListQueryVariables>, 'query'> = {}) {
  return Urql.useQuery<UserListQuery>({ query: UserListDocument, ...options });
};