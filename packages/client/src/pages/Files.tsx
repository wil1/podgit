import React, { useRef, useState } from 'react'
import { useMemo } from 'react'
import { Link, useRouteMatch, useParams, Switch, Route, Redirect } from 'react-router-dom'
import { useTable, useExpanded } from 'react-table'
import { useGroupListQuery, useImportUploadFileMutation, useUploadFileMutation, useUpsertGroupMutation } from '../generated/graphql'
import BasePage from '../layout/BasePage'
import Menu from '../layout/Menu'
import { format, parseJSON } from 'date-fns'
import { useForm } from 'react-hook-form'
import useStyles from '../hooks/useStyles'
import memoize from 'fast-memoize'
import { FunctionComponent } from 'react'
import _uniqueId from 'lodash/uniqueId';
import Loading from '../features/icons/Loading'


export type FileUploadProps = {
    onUpload: (fileId: number, fileName: string) => void
}

export const FileUpload: FunctionComponent<FileUploadProps> = props => {
    const {children, onUpload, ...rest} = props
    
    const [ {data, error, fetching }, doUpload] = useUploadFileMutation()
    const formEl = useRef<HTMLFormElement>(null)
    const [inputId] = useState(_uniqueId('upload-label-'));
    const [fileName, setFileName] = useState(null)
    const [uploading, setUploading] = useState(false)
    
    function onFileSelected(fileName: string) {
        if (formEl?.current) {
            const fd = new FormData(formEl.current);
            const uploadFile = fd.get("uploadFile");
            setUploading(true)
            doUpload({
                input: {
                    fileUpload: {
                        uploadFile
                    }
                }
            }).then(({data, error}) =>{
                setUploading(false)
                if (error) {
                    throw new Error(JSON.stringify(error))
                }
                props.onUpload(data?.createFileUpload?.fileUpload?.id as number, fileName)
            })

        }
    }

    
    if (fileName) {
        return (<div {...rest}>{fileName}{uploading && <Loading className="inline stroke-current text-blue-600" />}</div>)
    }

    return (
            <form ref={formEl}>
                
                    <label htmlFor={inputId} {...rest}>{children}</label>
                    <input
                        type="file"
                        id={inputId}
                        name="uploadFile"
                        accept="*/*"
                        required
                        style={{display: `none`}}
                        onChange={(event: any) => {
                            const name =
                                event.target.files.length > 0
                                    ? event.target.files[0].name
                                    : "";
                            setFileName(name)
                            onFileSelected(name)
                        }}
                    />
            </form>
    )
}

function UploadFile() {
    const styles = useStyles()
    const { path, url, } = useRouteMatch()
    // const { register, handleSubmit, watch, errors } = useForm()
    const [ importResponse, doImport] = useImportUploadFileMutation()

    function onUpload(uploadFileId: number, fileName: string) {
        doImport({input: {uploadFileId }}).then(result =>{
            console.log(`TODO REMOVE THIS LOG ${JSON.stringify(result.data)}`)
        })
    }

    return <BasePage title={`Upload File`}>
        <div className="py-4 px-4 mt-10 sm:mt-0">
            <FileUpload onUpload={onUpload}>
                Click here to upload a file
            </FileUpload>
        </div>
    </BasePage>
}



function FilesPage() {
    const { path, url, } = useRouteMatch()
    return (
        <>
            <Switch>
                {/* <Route path={`${url}/new`}>
                    <AddGroup />
                </Route>
                <Route path={`${url}/:id`}>
                    <GroupDetail />
                </Route> */}
                <Route>
                    <UploadFile />
                </Route>
            </Switch>
        </>
    )

}

export default FilesPage
