import React from 'react'
import { useMemo } from 'react'
import { Link, useRouteMatch, useParams, Switch, Route, Redirect } from 'react-router-dom'
import { useTable } from 'react-table'
import { useUserListQuery } from '../generated/graphql'
import BasePage from '../layout/BasePage'
import Menu from '../layout/Menu'
import { format, parseJSON } from 'date-fns'

const filteredRoles = [`offline_access`, `uma_authorization`]

function UserDetail() {
    const { id } = useParams<{id: string}>()
    return <div>User detail: {id}</div>
}

function UserList() {
    const [{data}] = useUserListQuery()
    
    const tableData = (data?.users?.nodes || []) as any
    const tableCols = useMemo(() => [
        { Header: `Id`, accessor: `id` },
        { Header: `First Name`, accessor: `givenName` },
        { Header: `Last Name`, accessor: `familyName` },
        { Header: `User Name`, accessor: `preferredUsername` },
        { Header: `Email`, accessor: `email` },
        // { Header: `External Id`, accessor: `externalId` },
        { Header: `Last Login`, accessor: (row:any) =>  {
            const raw = row.lastLogin
            console.log(`formatting value ***${raw}***`)

            const parsed = parseJSON(raw)
            return format(parsed, `MM/dd/yyyy`)
        }},
        { Header: `Roles`, accessor: 
            (row:any) => row.roles.filter(
                (role:any) => filteredRoles.indexOf(role) === -1
            ).join(`,`)
        }
    ], [])
    const tableInstance = useTable({columns: tableCols, data: tableData})
    
    const {
        getTableProps,
        getTableBodyProps,
        headerGroups,
        rows,
        prepareRow,
    } = tableInstance

    return (
        <BasePage title="User List">
            <div className="align-middle inline-block min-w-full shadow overflow-hidden bg-white shadow-dashboard px-8 pt-3 rounded-bl-lg rounded-br-lg">
                <table className="min-w-full" {...getTableProps()}>
                    <thead>
                        {headerGroups.map(headerGroup => (
                            <tr {...headerGroup.getHeaderGroupProps()}>
                                {headerGroup.headers.map(column => (
                                    <th {...column.getHeaderProps({
                                            className: "text-left px-4 py-2 border-b-2 border-gray-300 text-left leading-4 text-blue-800"
                                        })}>
                                            {console.log(`HEAER COLUMN`, column)}
                                        {column.render('Header')}
                                    </th>
                                ))}
                            </tr>
                        ))}
                    </thead>
                    <tbody {...getTableBodyProps()}>
                        {rows.map(row => {
                            prepareRow(row)
                            return (
                                <tr {...row.getRowProps()}>
                                    {row.cells.map(cell => {
                                        return (
                                            <td {...cell.getCellProps({
                                                className: "px-4 py-2 whitespace-no-wrap border-b border-gray-500"
                                            })}>
                                                {cell.render('Cell')}
                                            </td>
                                        )
                                    })}
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
            </div>
        </BasePage>
    )
}

function UsersPage() {
    const {path, url, } = useRouteMatch()    
    return (
        <>
            <Switch>
                <Route path={`${url}/:id`}>
                    <UserDetail />
                </Route>
                <Route>
                    <UserList />
                </Route>
            </Switch>
        </>
    )

}

export default UsersPage
    