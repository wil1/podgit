import React from 'react'

import { useAuth } from '../features/auth/useAuth'
import { MenuItemType, useMenuItems } from  '../hooks/useMenuItems'
import { Link } from 'react-router-dom'
import BasePage from '../layout/BasePage'

function LinkCard(props: {item: MenuItemType}) {
    const {color, icon, link, title} = props.item
    return (
            <div className="w-full md:w-1/2 xl:w-1/3 p-6">
        <Link to={link}>
                <div className={`bg-gradient-to-b from-${color}-200 to-${color}-100 border-b-4 border-${color}-600 rounded-lg shadow-xl p-5`}>
                    <div className="flex flex-row items-center">
                        <div className="flex-shrink pr-4">
                            <div className={`rounded-full p-5 bg-${color}-600`}><i className={`fa ${icon} fa-2x fa-inverse`}></i></div>
                        </div>
                        <div className="flex-1 text-right md:text-center">
                            <h5 className="font-bold uppercase text-gray-600">{title}</h5>
                        </div>
                    </div>
                </div>
        </Link>
            </div>
    )
}


function Home() {
    const { mainMenu } = useMenuItems()
    
    return (
        <BasePage title="Home">
            {mainMenu.map(item => <LinkCard item={item}/>)}
        </BasePage>
    )
}

export default Home
    