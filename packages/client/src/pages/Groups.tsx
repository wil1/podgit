import React, { useCallback } from 'react'
import { useMemo } from 'react'
import { Link, useRouteMatch, useParams, Switch, Route, Redirect } from 'react-router-dom'
import { useTable, useExpanded } from 'react-table'
import { useGroupListQuery, useUpsertGroupMutation } from '../generated/graphql'
import BasePage from '../layout/BasePage'
import Menu from '../layout/Menu'
import { format, parseJSON } from 'date-fns'
import { useForm } from 'react-hook-form'
import useStyles from '../hooks/useStyles'
import memoize from 'fast-memoize'


function GroupDetail() {
    const { id } = useParams<{ id: string }>()
    return <div>Group detail: {id}</div>
}


function AddGroup() {
    const styles = useStyles()
    const { path, url, } = useRouteMatch()
    const { register, handleSubmit, watch, errors } = useForm()
    const [{ error, data }, upsert] = useUpsertGroupMutation()

    const onSubmit = (data: any) => {
        const { name, owner } = data
        upsert({ input: { name, owner } })
    }

    if (data?.upsertGroup?.group?.id) {
        return <Redirect to={`/groups`} />
    }

    return <BasePage title={`Add New Group`}>
        <div className="py-4 px-4 mt-10 sm:mt-0">
            <form onSubmit={handleSubmit(onSubmit)}>
                <div className="md:grid md:grid-cols-3 md:gap-6">
                    <div className="mt-5 md:mt-0 md:col-span-2">
                        <div className="shadow overflow-hidden sm:rounded-md">
                            <div className="px-4 py-5 bg-white sm:p-6">
                                <div className="grid grid-cols-6 gap-6">
                                    <div className="col-span-6 sm:col-span-3">
                                        <label htmlFor="name" className={styles.forms.label}>Group name</label>
                                        <input type="text" name="name" id="name" className={styles.forms.input} ref={register({ required: true })} />
                                        {errors.name && <span>Group name is required</span>}
                                    </div>

                                    <div className="col-span-6 sm:col-span-3">
                                        <label htmlFor="owner" className={styles.forms.label}>Group owner</label>
                                        <input type="text" name="owner" id="owner" className={styles.forms.input} ref={register({ required: true })} />
                                        {errors.owner && <span>Group owner is required</span>}
                                    </div>

                                </div>
                            </div>
                            <div className="px-4 py-3 bg-gray-50 text-right sm:px-6">
                                <input type="submit" className={styles.buttons.primary} value="Save" />
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </BasePage>
}

function ExpandIcon() {
    return (<i className="fas fa-chevron-right"></i>)
}
function CollapseIcon() {
    return (<i className="fas fa-chevron-down"></i>)
}

function GroupList() {
    const styles = useStyles()
    const { path, url, } = useRouteMatch()
    const [{ data, fetching }] = useGroupListQuery()

    const tableData = useMemo(() => (data?.groups?.nodes as any) || [], [fetching])


    const tableCols = useMemo(() => [
        {
            id: 'expander',
            Header: '',
            Cell: ({ row }: any) => row.isExpanded ? <CollapseIcon /> : <ExpandIcon />
        },
        { Header: `Id`, accessor: `id` },
        { Header: `Name`, accessor: `name` },
        {
            Header: `Owner`, accessor: (row: any) => {
                const raw = row.owner
                return raw || `nobody here`
            }
        },
    ], [])


    const renderRowSubComponent = React.useCallback(
        ({ row }) => (
            <div>Subgroups: {row.original.subgroups.nodes.map((sub: any) => sub.subgroup?.name).join(`,`)}</div>
        ),
        []
    )

    const tableInstance = useTable({
        columns: tableCols,
        data: tableData
    }, useExpanded)

    const {
        getTableProps,
        getTableBodyProps,
        headerGroups,
        rows,
        prepareRow,
        visibleColumns,
    } = tableInstance

    const expanded = (tableInstance as any).state?.expanded

    return (
        <BasePage title="Group List">
            <div className="align-middle inline-block min-w-full shadow overflow-hidden bg-white shadow-dashboard px-8 pt-3 rounded-bl-lg rounded-br-lg">
                <table className="min-w-full" {...getTableProps()}>
                    <thead>
                        {headerGroups.map(headerGroup => (
                            <tr {...headerGroup.getHeaderGroupProps()}>
                                {headerGroup.headers.map(column => (
                                    <th {...column.getHeaderProps({
                                        className: "text-left px-4 py-2 border-b-2 border-gray-300 text-left leading-4 text-blue-800"
                                    })}>
                                        {column.render('Header')}
                                    </th>
                                ))}
                            </tr>
                        ))}
                    </thead>
                    <tbody {...getTableBodyProps()}>
                        {rows.map((row: any) => {
                            prepareRow(row)
                            const { key, role } = row.getRowProps()
                            return (

                                // Use a React.Fragment here so the table markup is still valid
                                <React.Fragment key={key}>
                                    <tr {...row.getToggleRowExpandedProps({})} role={role}>
                                        {row.cells.map((cell: any) => {
                                            return (
                                                <td className="px-4 py-2 whitespace-no-wrap border-b border-gray-500" {...cell.getCellProps()}>{cell.render('Cell')}</td>
                                            )
                                        })}
                                    </tr>
                                    {/*
                                        If the row is in an expanded state, render a row with a
                                        column that fills the entire length of the table.
                                    */}
                                    {row.isExpanded ? (
                                        <tr role={role}>
                                            <td colSpan={visibleColumns.length} className="px-4 py-2 whitespace-no-wrap border-b border-gray-500">
                                                {/*
                                                Inside it, call our renderRowSubComponent function. In reality,
                                                you could pass whatever you want as props to
                                                a component like this, including the entire
                                                table instance. But for this example, we'll just
                                                pass the row
                                                */}
                                                {renderRowSubComponent({ row })}
                                            </td>
                                        </tr>
                                    ) : null}
                                </React.Fragment>

                            )
                        })}
                    </tbody>
                    <tfoot>
                        <tr>
                            <td className="py-3" colSpan={visibleColumns.length}>
                                <Link to={`${url}/new`}>
                                    <button className={styles.buttons.primary}>Add Group</button>
                                </Link>
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </BasePage>
    )
}

function GroupsPage() {
    const { path, url, } = useRouteMatch()
    return (
        <>
            <Switch>
                <Route path={`${url}/new`}>
                    <AddGroup />
                </Route>
                <Route path={`${url}/:id`}>
                    <GroupDetail />
                </Route>
                <Route>
                    <GroupList />
                </Route>
            </Switch>
        </>
    )

}

export default GroupsPage
