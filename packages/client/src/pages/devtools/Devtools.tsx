import React from 'react'
import { Link, useRouteMatch, useParams, Switch, Route, Redirect } from 'react-router-dom'
import { DevtoolsMenuItemType, useMenuItems } from '../../hooks/useMenuItems'
import Menu from '../../layout/Menu'
import GraphiqlPage from './GraphiqlPage'

function ToolTabs() {
    const { devtoolsMenu } = useMenuItems()
    const { tab } = useParams<{tab: string}>()

    const isActiveItem = (item: DevtoolsMenuItemType) => {
        console.log(`checking if "${tab}" = "${item.link}"`)
        return `${tab}` === item.link
    }

    return (
        <ul className="list-reset flex border-b">
                <li key="home" className="mr-1">
                    <Link to={`/home`}
                        className="bg-white inline-block py-2 px-4 text-blue hover:text-blue-darker">
                        Home
                    </Link>                
                </li>

                {devtoolsMenu.map(item => isActiveItem(item) ? 
                    <li key={item.title} className="-mb-px mr-1">
                        <Link to={`/devtools/${item.link}`}
                            className="bg-white inline-block border-l border-t border-r rounded-t py-2 px-4 text-blue-dark font-semibold">
                            {item.title}
                        </Link>
                    </li>
                    : 
                    <li key={item.title} className="mr-1">
                        <Link to={`/devtools/${item.link}`}
                            className="bg-white inline-block py-2 px-4 text-blue hover:text-blue-darker">
                            {item.title}
                        </Link>                
                    </li>
                )}            
            </ul>
    )
}

function Devtools() {
    const {path, url, } = useRouteMatch()    
    return (
        <>
            <Route path={`${url}/:tab`}>
                <ToolTabs />
            </Route>
            <Switch>
                <Route path={`${url}/graphiql`}>
                    <GraphiqlPage />
                </Route>
                <Route path={`${url}/generate`}>
                    <div>TODO add code generator</div>
                </Route>
                <Route>
                    <Redirect to={`${url}/graphiql`} />
                </Route>
            </Switch>
        </>
    )
}

export default Devtools
    