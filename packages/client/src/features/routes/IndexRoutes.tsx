import React from 'react'
import {
  Redirect,
  Route,
  Switch,
  useRouteMatch
} from 'react-router-dom'
import loadable from '@loadable/component'
import App from '../../App';
import LandingPage from '../../pages/Landing';

const Devtools = loadable(() => import('../../pages/devtools/Devtools'))


function IndexRoutes() {
    return (
        <Switch>
            <Route path="/devtools">
                <Devtools />
            </Route>
            <Route exact path="/welcome">
                <LandingPage/>
            </Route>
            <Route>
                <App />
            </Route>
        </Switch>
    )

    
}

export default IndexRoutes
    