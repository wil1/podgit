import { useKeycloak } from '@react-keycloak/web'
import { useEffect, useCallback } from 'react'
import * as Urql from 'urql';
import gql from 'graphql-tag';

export type User = {
    id: number
    externalId: string
    preferredUsername: string,
    jwt: any
}

export type AuthInfo = {
    user?: User
    token?: string
}

export type AuthInfoState = {
    auth: AuthInfo
    login: () => void
    logout: () => void
    hasRole: (role: string) => boolean
}

function makePostOptions(bodyContent: any) {
    return {
        method: 'POST', // *GET, POST, PUT, DELETE, etc.
        mode: 'cors', // no-cors, *cors, same-origin
        cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        credentials: 'include', // include, *same-origin, omit
        headers: {
        'Content-Type': 'application/json'
        // 'Content-Type': 'application/x-www-form-urlencoded',
        },
        redirect: 'follow', // manual, *follow, error
        referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
        body: JSON.stringify(bodyContent) // body data type must match "Content-Type" header
  } as any
}

export const useAuth = (overrides?: Partial<AuthInfo>): AuthInfoState => {    
    const [registerResponse, register] = Urql.useMutation(gql`
        mutation RegisterUser($input: RegisterUserInput!) {
            registerUser(input: $input) {
                userSessions {
                id
                externalId
                preferredUsername
                jwt
                }
            }
        }
    `)

    const userSessions = registerResponse.data?.registerUser
        ?.userSessions as any
    const user = userSessions && (userSessions[0] as User)
    
    const {keycloak} = useKeycloak()
    
    const login = useCallback(() => {
        if (keycloak) {
            keycloak.login({ scope: 'profile' })
        }
    }, [keycloak])

    const logout = useCallback(() => {
        if (keycloak) {
            keycloak.logout()
        }
    }, [keycloak])


    useEffect(() => {
        fetch(`/register-token`, makePostOptions({
            jwt: keycloak?.token
        })).then(() => {
            if (keycloak?.token) {
                register({ input: {} })
            }
        })
        
        return () => {}
    }, [keycloak?.token])

   
    const hasRole = (role: string) =>
        (user && user.jwt.realm_access.roles.indexOf(role) !== -1) ||
        false

    return {auth: {user, token: keycloak?.token}, login, logout, hasRole }
}










