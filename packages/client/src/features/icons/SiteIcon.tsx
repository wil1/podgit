import * as React from "react"

function SiteIcon(props:any) {
  return (
    <svg
      id="prefix__Layer_1"
      data-name="Layer 1"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <defs>
        <style>{".prefix__cls-1{fill:#2d3e50}"}</style>
      </defs>
      <title>{"b"}</title>
      <path
        className="prefix__cls-1"
        d="M48.571 16.283h30.858v2.908H48.571zM48.571 27.099h30.858v2.908H48.571z"
      />
      <path
        d="M24.794 84.29a2.714 2.714 0 002.713-2.711V69.857h72.988V81.58a2.711 2.711 0 005.422 0V67.146a2.714 2.714 0 00-2.711-2.71H66.809V52.714a2.71 2.71 0 00-5.422 0v11.722H24.794a2.714 2.714 0 00-2.71 2.71V81.58a2.714 2.714 0 002.71 2.71z"
      />
      <path
        className="prefix__cls-1"
        d="M30.952 91.947h-12.17a16.517 16.517 0 100 33.034h12.17a16.517 16.517 0 000-33.034zM109.22 91.947H97.052a16.517 16.517 0 100 33.034h12.17a16.517 16.517 0 000-33.034zM93.504 3.02H34.496v40.25h59.008zM85.75 35.516h-43.5V10.773h43.5z"
      />
    </svg>
  )
}

export default SiteIcon
