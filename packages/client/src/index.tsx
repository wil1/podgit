import React from 'react';
import ReactDOM from 'react-dom';
import {
  BrowserRouter as Router,
} from 'react-router-dom'

import './index.css';
import reportWebVitals from './reportWebVitals';
import { AuthProvider} from './features/auth/AuthProvider'
import IndexRoutes from './features/routes/IndexRoutes';



ReactDOM.render(
  <React.StrictMode>
    <AuthProvider 
        authDomain={process.env.REACT_APP_CYTON_AUTH_DOMAIN || ``}
        apiDomain={process.env.REACT_APP_CYTON_API_DOMAIN || ``}
        realm={process.env.REACT_APP_CYTON_PROJECT_NAME || ``}
        clientId={process.env.REACT_APP_CYTON_CLIENT_ID || ``} >
        <Router>
            <IndexRoutes />
        </Router>
    </AuthProvider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
