# FROM postgres:12 AS build

# ENV WAL2JSON_COMMIT_SHA=da90c76a69966a7dfcf0657acacde916164bd9c0

# # Install the packages which will be required to get everything to compile
# RUN apt-get update && \
#     apt-get install -f -y --no-install-recommends \
#         software-properties-common \
#         build-essential \
#         pkg-config \
#         git \
#         postgresql-server-dev-$PG_MAJOR

# RUN git clone https://github.com/eulerto/wal2json -b master --single-branch && \
#     cd /wal2json && \
#     git checkout $WAL2JSON_COMMIT_SHA && \
#     make && make install



FROM gitpod/workspace-postgres

USER root

# COPY --from=build /usr/lib/postgresql/12/lib/wal2json.so /usr/lib/postgresql/12/lib/
# COPY packages/postgres/postgresql.conf.sample /usr/share/postgresql/postgresql.conf.sample

RUN mkdir -p /var/lib/pgadmin && \
    mkdir -p /var/log/pgadmin && \
    chown gitpod /var/lib/pgadmin && \
    chown gitpod /var/log/pgadmin

RUN apt-get update && \
    apt-get install -y build-essential libssl-dev libffi-dev libgmp3-dev python3-virtualenv libpq-dev python3-dev netcat && \
    apt-get clean

RUN pip install pgadmin4


# for debugging
RUN apt-get update && \
    apt-get install -y mlocate && \
    apt-get clean && \
    updatedb

USER gitpod
