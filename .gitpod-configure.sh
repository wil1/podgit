#!/bin/bash
export CYTON_PROJECT_NAME=podgit

echo "DANGEROUSLY_DISABLE_HOST_CHECK=true" > /workspace/$CYTON_PROJECT_NAME/packages/client/.env

export REACT_APP_CYTON_PROJECT_NAME=$CYTON_PROJECT_NAME
export REACT_APP_CYTON_ROOT_URL=$(gp url 3000)
export REACT_APP_CYTON_CLIENT_ID="gitpod-${GITPOD_WORKSPACE_ID}"
export CYTON_AUTH_DOMAIN=auth.gitpod.cyton.net
export CYTON_API_DOMAIN=$(gp url 4000 | cut -c9-)
export REACT_APP_CYTON_AUTH_DOMAIN=$CYTON_AUTH_DOMAIN
export REACT_APP_CYTON_API_DOMAIN=$CYTON_API_DOMAIN

eval $(gp env --export)

if [ -z "$CYTON_DEV_USER" ] ; then
    gp env CYTON_DEV_USER=cyton
fi

if [ -z "$CYTON_DEV_PASSWORD" ] ; then
    gp env CYTON_DEV_PASSWORD=$(date | md5sum | head -c 32)
fi


eval $(gp env --export)

curl -G -v "https://init.gitpod.cyton.net/api/realms/init" \
    --data-urlencode "realmName=${CYTON_PROJECT_NAME}" \
    --data-urlencode "rootUrl=${REACT_APP_CYTON_ROOT_URL}" \
    --data-urlencode "realmUserName=${CYTON_DEV_USER}" \
    --data-urlencode "realmUserPassword=${CYTON_DEV_PASSWORD}" \
    --data-urlencode "roles=ADMIN,DEVELOPER" \
    --data-urlencode "gitpodWorkspaceId=${GITPOD_WORKSPACE_ID}"
      
export CYTON_UPLOAD_DIRECTORY=/workspace/uploads
mkdir -p $CYTON_UPLOAD_DIRECTORY

export CYTON_DATA_DIRECTORY=/workspace/data
mkdir -p $CYTON_DATA_DIRECTORY
